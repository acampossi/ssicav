# -*- mode: python -*-

block_cipher = None

options = [ ('v', None, 'OPTION'), ('W ignore', None, 'OPTION') ]
a = Analysis(['../Repositorio/Interface/Index.py'],
             pathex=['../Repositorio/Interface', '/usr/local/lib/python2.7/dist-packages'],
             binaries=[],
             datas=[('/home/ssi_ralvarez/Documentos/all_project/Repositorio/Interface/Backend/parametros','./Backend'),
                    ('/home/ssi_ralvarez/Documentos/all_project/Repositorio/Interface/Backend/PlatesDetectors/*.xml','./Backend/PlatesDetectors'),
                    ('/home/ssi_ralvarez/Documentos/all_project/Repositorio/Interface/Backend/PlatesReaders/*.h5','./Backend/PlatesReaders'),
                    ('/home/ssi_ralvarez/Documentos/all_project/Repositorio/Interface/Backend/parametros_rplidar','./Backend'),
                    ('/home/ssi_ralvarez/Documentos/all_project/Repositorio/Interface/Backend/default_parametros','./Backend'),
                    ('/home/ssi_ralvarez/Documentos/all_project/Repositorio/Interface/Backend/default_parametros_rplidar','./Backend'),
                    ('/home/ssi_ralvarez/Documentos/all_project/Repositorio/Interface/Prueba_FPFH_camion','Prueba_FPFH_camion'),
                    ('/home/ssi_ralvarez/Documentos/all_project/Repositorio/Interface/Rplidar_Fpfh_v3','Rplidar_Fpfh_v3'),
                    ('/home/ssi_ralvarez/Documentos/all_project/Repositorio/Interface/Nubes_Puntos','Nubes_Puntos'),
                    ('/home/ssi_ralvarez/Documentos/all_project/Repositorio/Interface/VisualizationWidgets/*.png','./VisualizationWidgets'),
                    ('/home/ssi_ralvarez/Documentos/all_project/Repositorio/Interface/VisualizationWidgets/*.jpg','./VisualizationWidgets'),
                    ('/home/ssi_ralvarez/Documentos/all_project/RepositoriosExternos/Vimba_2_1/VimbaGigETL/CTI/x86_64bit','./Vimba_2_1/VimbaGigETL/CTI/x86_64bit'),
                    ('/home/ssi_ralvarez/Documentos/all_project/RepositoriosExternos/Vimba_2_1/VimbaC/DynamicLib/x86_64bit','./Vimba_2_1/VimbaC/DynamicLib/x86_64bit')],
             hiddenimports=['six','sklearn.neighbors.typedefs','sklearn.tree._utils', '_pyqt4', 'vispy' , 'vispy.vispy' ,'vispy.app.backends._pyqt4', 'PyQt4', 'PyQt4.uic,'],
             hookspath=[],
             runtime_hooks=[],
             excludes=[],
             win_no_prefer_redirects=False,
             win_private_assemblies=False,
             cipher=block_cipher)
pyz = PYZ(a.pure, a.zipped_data,
             cipher=block_cipher)
exe = EXE(pyz,
          a.scripts,
          exclude_binaries=True,
          name='AFORO_SSI_SENA',
          debug=False,
          strip=False,
          upx=True,
          console=True )
coll = COLLECT(exe,
               a.binaries,
               a.zipfiles,
               a.datas,
               strip=False,
               upx=True,
               name='AFORO_SSI_SENA')
