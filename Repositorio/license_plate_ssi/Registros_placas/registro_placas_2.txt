{
    "1.png": {
        "Placa_sistema": 0,
        "Placas_usuario": "CFI801",
        "Pos_sistema": [
            {
                "x": 595,
                "y": 318
            },
            {
                "x": 707,
                "y": 319
            },
            {
                "x": 707,
                "y": 378
            },
            {
                "x": 595,
                "y": 376
            }
        ],
        "Pos_usuario": [
            593,
            325,
            102,
            58
        ]
    },
    "10.png": {
        "Placa_sistema": 0,
        "Placa_usuario": "JWE960",
        "Pos_sistema": 0,
        "Pos_usuario": [
            314,
            311,
            118,
            89
        ]
    },
    "11.png": {
        "Placa_sistema": 0,
        "Placas_usuario": "DIS086",
        "Pos_sistema": [
            {
                "x": 548,
                "y": 297
            },
            {
                "x": 682,
                "y": 297
            },
            {
                "x": 682,
                "y": 363
            },
            {
                "x": 548,
                "y": 362
            }
        ],
        "Pos_usuario": [
            570,
            295,
            117,
            74
        ]
    },
    "12.png": {
        "Placa_sistema": 0,
        "Placa_usuario": "0",
        "Pos_sistema": 0,
        "Pos_usuario": [
            0,
            0,
            0,
            0
        ]
    },
    "13.png": {
        "Placa_sistema": 0,
        "Placa_usuario": "MCD045",
        "Pos_sistema": 0,
        "Pos_usuario": [
            543,
            248,
            75,
            54
        ]
    },
    "14.png": {
        "Placa_sistema": "CPM366",
        "Placa_usuario": "CPM366",
        "Pos_sistema": [
            {
                "x": 490,
                "y": 310
            },
            {
                "x": 571,
                "y": 312
            },
            {
                "x": 572,
                "y": 362
            },
            {
                "x": 489,
                "y": 361
            }
        ],
        "Pos_usuario": [
            488,
            311,
            96,
            61
        ]
    },
    "15.png": {
        "Placa_sistema": 0,
        "Placa_usuario": "0",
        "Pos_sistema": 0,
        "Pos_usuario": [
            0,
            0,
            0,
            0
        ]
    },
    "16.png": {
        "Placa_sistema": "VCR626",
        "Placa_usuario": "VCR626",
        "Pos_sistema": [
            {
                "x": 554,
                "y": 282
            },
            {
                "x": 703,
                "y": 282
            },
            {
                "x": 703,
                "y": 359
            },
            {
                "x": 554,
                "y": 359
            }
        ],
        "Pos_usuario": [
            556,
            289,
            115,
            74
        ]
    },
    "17.png": {
        "Placa_sistema": 0,
        "Placa_usuario": "0",
        "Pos_sistema": 0,
        "Pos_usuario": [
            0,
            0,
            0,
            0
        ]
    },
    "18.png": {
        "Placa_sistema": 0,
        "Placas_usuario": "HZU364",
        "Pos_sistema": [
            {
                "x": 562,
                "y": 298
            },
            {
                "x": 683,
                "y": 296
            },
            {
                "x": 686,
                "y": 353
            },
            {
                "x": 565,
                "y": 354
            }
        ],
        "Pos_usuario": [
            586,
            300,
            102,
            60
        ]
    },
    "19.png": {
        "Placa_sistema": 0,
        "Placas_usuario": "VKI794",
        "Pos_sistema": [
            {
                "x": 449,
                "y": 314
            },
            {
                "x": 570,
                "y": 320
            },
            {
                "x": 562,
                "y": 399
            },
            {
                "x": 449,
                "y": 391
            }
        ],
        "Pos_usuario": [
            440,
            320,
            132,
            84
        ]
    },
    "2 (1).png": {
        "Placa_sistema": 0,
        "Placas_usuario": "CPW721",
        "Pos_sistema": [
            {
                "x": 888,
                "y": 306
            },
            {
                "x": 949,
                "y": 308
            },
            {
                "x": 949,
                "y": 340
            },
            {
                "x": 888,
                "y": 338
            }
        ],
        "Pos_usuario": [
            877,
            309,
            68,
            33
        ]
    },
    "2.png": {
        "Placa_sistema": 0,
        "Placas_usuario": "CPW721",
        "Pos_sistema": [
            {
                "x": 888,
                "y": 306
            },
            {
                "x": 949,
                "y": 308
            },
            {
                "x": 949,
                "y": 340
            },
            {
                "x": 888,
                "y": 338
            }
        ],
        "Pos_usuario": [
            877,
            309,
            70,
            32
        ]
    },
    "20.png": {
        "Placa_sistema": 0,
        "Placa_usuario": "0",
        "Pos_sistema": 0,
        "Pos_usuario": [
            0,
            0,
            0,
            0
        ]
    },
    "21.png": {
        "Placa_sistema": "HJT318",
        "Placa_usuario": "HJT318",
        "Pos_sistema": [
            {
                "x": 691,
                "y": 294
            },
            {
                "x": 773,
                "y": 294
            },
            {
                "x": 773,
                "y": 352
            },
            {
                "x": 692,
                "y": 352
            }
        ],
        "Pos_usuario": [
            690,
            296,
            87,
            48
        ]
    },
    "22.png": {
        "Placa_sistema": 0,
        "Placa_usuario": "CPV716",
        "Pos_sistema": 0,
        "Pos_usuario": [
            633,
            309,
            113,
            65
        ]
    },
    "23.png": {
        "Placa_sistema": 0,
        "Placa_usuario": "0",
        "Pos_sistema": 0,
        "Pos_usuario": [
            0,
            0,
            0,
            0
        ]
    },
    "24.png": {
        "Placa_sistema": "CUK959",
        "Placa_usuario": "CUK959",
        "Pos_sistema": [
            {
                "x": 491,
                "y": 225
            },
            {
                "x": 632,
                "y": 210
            },
            {
                "x": 632,
                "y": 284
            },
            {
                "x": 490,
                "y": 299
            }
        ],
        "Pos_usuario": [
            527,
            222,
            108,
            64
        ]
    },
    "25.png": {
        "Placa_sistema": 0,
        "Placa_usuario": "FLD986",
        "Pos_sistema": 0,
        "Pos_usuario": [
            743,
            291,
            91,
            51
        ]
    },
    "26.png": {
        "Placa_sistema": 0,
        "Placas_usuario": "MJO930",
        "Pos_sistema": [
            {
                "x": 598,
                "y": 294
            },
            {
                "x": 708,
                "y": 295
            },
            {
                "x": 707,
                "y": 351
            },
            {
                "x": 597,
                "y": 350
            }
        ],
        "Pos_usuario": [
            599,
            295,
            109,
            64
        ]
    },
    "27.png": {
        "Placa_sistema": 0,
        "Placas_usuario": "CLT813",
        "Pos_sistema": [
            {
                "x": 679,
                "y": 310
            },
            {
                "x": 791,
                "y": 315
            },
            {
                "x": 799,
                "y": 371
            },
            {
                "x": 686,
                "y": 366
            }
        ],
        "Pos_usuario": [
            682,
            318,
            94,
            56
        ]
    },
    "28.png": {
        "Placa_sistema": 0,
        "Placas_usuario": "CLT813",
        "Pos_sistema": [
            {
                "x": 506,
                "y": 323
            },
            {
                "x": 642,
                "y": 328
            },
            {
                "x": 639,
                "y": 393
            },
            {
                "x": 503,
                "y": 391
            }
        ],
        "Pos_usuario": [
            541,
            331,
            105,
            66
        ]
    },
    "29.png": {
        "Placa_sistema": 0,
        "Placa_usuario": "CAB964",
        "Pos_sistema": 0,
        "Pos_usuario": [
            355,
            323,
            108,
            88
        ]
    },
    "3.png": {
        "Placa_sistema": 0,
        "Placas_usuario": "CPW721",
        "Pos_sistema": [
            {
                "x": 391,
                "y": 355
            },
            {
                "x": 526,
                "y": 364
            },
            {
                "x": 540,
                "y": 424
            },
            {
                "x": 404,
                "y": 412
            }
        ],
        "Pos_usuario": [
            386,
            356,
            109,
            69
        ]
    },
    "30.png": {
        "Placa_sistema": "KID338",
        "Placa_usuario": "KID338",
        "Pos_sistema": [
            {
                "x": 534,
                "y": 287
            },
            {
                "x": 687,
                "y": 285
            },
            {
                "x": 689,
                "y": 356
            },
            {
                "x": 537,
                "y": 358
            }
        ],
        "Pos_usuario": [
            533,
            291,
            128,
            78
        ]
    },
    "31.png": {
        "Placa_sistema": 0,
        "Placa_usuario": "0",
        "Pos_sistema": 0,
        "Pos_usuario": [
            0,
            0,
            0,
            0
        ]
    },
    "32 (1).png": {
        "Placa_sistema": "UBU706",
        "Placa_usuario": "UBU706",
        "Pos_sistema": [
            {
                "x": 424,
                "y": 286
            },
            {
                "x": 529,
                "y": 288
            },
            {
                "x": 525,
                "y": 357
            },
            {
                "x": 424,
                "y": 353
            }
        ],
        "Pos_usuario": [
            423,
            286,
            107,
            79
        ]
    },
    "32.png": {
        "Placa_sistema": "UBU706",
        "Placa_usuario": "UBU706",
        "Pos_sistema": [
            {
                "x": 424,
                "y": 286
            },
            {
                "x": 529,
                "y": 288
            },
            {
                "x": 525,
                "y": 357
            },
            {
                "x": 424,
                "y": 353
            }
        ],
        "Pos_usuario": [
            420,
            289,
            112,
            72
        ]
    },
    "33.png": {
        "Placa_sistema": "TZO629",
        "Placa_usuario": "TZO629",
        "Pos_sistema": [
            {
                "x": 575,
                "y": 291
            },
            {
                "x": 662,
                "y": 292
            },
            {
                "x": 662,
                "y": 353
            },
            {
                "x": 574,
                "y": 352
            }
        ],
        "Pos_usuario": [
            569,
            299,
            101,
            62
        ]
    },
    "34.png": {
        "Placa_sistema": "KLU255",
        "Placa_usuario": "KLU255",
        "Pos_sistema": [
            {
                "x": 618,
                "y": 286
            },
            {
                "x": 716,
                "y": 288
            },
            {
                "x": 716,
                "y": 344
            },
            {
                "x": 616,
                "y": 342
            }
        ],
        "Pos_usuario": [
            615,
            292,
            102,
            61
        ]
    },
    "35.png": {
        "Placa_sistema": 0,
        "Placa_usuario": "BGQ008",
        "Pos_sistema": 0,
        "Pos_usuario": [
            391,
            310,
            111,
            75
        ]
    },
    "36 (1).png": {
        "Placa_sistema": "FUC834",
        "Placa_usuario": "FUC834",
        "Pos_sistema": [
            {
                "x": 934,
                "y": 307
            },
            {
                "x": 1067,
                "y": 311
            },
            {
                "x": 1067,
                "y": 375
            },
            {
                "x": 934,
                "y": 373
            }
        ],
        "Pos_usuario": [
            934,
            314,
            137,
            62
        ]
    },
    "36.png": {
        "Placa_sistema": "FUC834",
        "Placa_usuario": "FUC834",
        "Pos_sistema": [
            {
                "x": 934,
                "y": 307
            },
            {
                "x": 1067,
                "y": 311
            },
            {
                "x": 1067,
                "y": 375
            },
            {
                "x": 934,
                "y": 373
            }
        ],
        "Pos_usuario": [
            934,
            314,
            138,
            64
        ]
    },
    "37.png": {
        "Placa_sistema": "HDY171",
        "Placa_usuario": "HDY171",
        "Pos_sistema": [
            {
                "x": 662,
                "y": 300
            },
            {
                "x": 765,
                "y": 303
            },
            {
                "x": 765,
                "y": 357
            },
            {
                "x": 663,
                "y": 356
            }
        ],
        "Pos_usuario": [
            661,
            302,
            107,
            60
        ]
    },
    "38.png": {
        "Placa_sistema": "CKF833",
        "Placa_usuario": "CKF833",
        "Pos_sistema": [
            {
                "x": 477,
                "y": 320
            },
            {
                "x": 620,
                "y": 321
            },
            {
                "x": 620,
                "y": 386
            },
            {
                "x": 477,
                "y": 385
            }
        ],
        "Pos_usuario": [
            512,
            332,
            112,
            64
        ]
    },
    "39.png": {
        "Placa_sistema": 0,
        "Placa_usuario": "ELM150",
        "Pos_sistema": 0,
        "Pos_usuario": [
            646,
            298,
            92,
            52
        ]
    },
    "4.png": {
        "Placa_sistema": 0,
        "Placa_usuario": "CUP480",
        "Pos_sistema": 0,
        "Pos_usuario": [
            902,
            280,
            69,
            32
        ]
    },
    "40.png": {
        "Placa_sistema": 0,
        "Placa_usuario": "0",
        "Pos_sistema": 0,
        "Pos_usuario": [
            0,
            0,
            0,
            0
        ]
    },
    "41.png": {
        "Placa_sistema": "CNA391",
        "Placa_usuario": "CNA391",
        "Pos_sistema": [
            {
                "x": 672,
                "y": 308
            },
            {
                "x": 756,
                "y": 313
            },
            {
                "x": 754,
                "y": 365
            },
            {
                "x": 672,
                "y": 360
            }
        ],
        "Pos_usuario": [
            671,
            316,
            84,
            53
        ]
    },
    "42.png": {
        "Placa_sistema": "VCJ029",
        "Placa_usuario": "VCJ029",
        "Pos_sistema": [
            {
                "x": 506,
                "y": 301
            },
            {
                "x": 612,
                "y": 301
            },
            {
                "x": 612,
                "y": 371
            },
            {
                "x": 505,
                "y": 371
            }
        ],
        "Pos_usuario": [
            506,
            299,
            106,
            68
        ]
    },
    "43.png": {
        "Placa_sistema": "VCV672",
        "Placa_usuario": "VCV672",
        "Pos_sistema": [
            {
                "x": 734,
                "y": 292
            },
            {
                "x": 840,
                "y": 294
            },
            {
                "x": 840,
                "y": 337
            },
            {
                "x": 734,
                "y": 335
            }
        ],
        "Pos_usuario": [
            733,
            292,
            101,
            51
        ]
    },
    "44.png": {
        "Placa_sistema": 0,
        "Placa_usuario": "0",
        "Pos_sistema": 0,
        "Pos_usuario": [
            0,
            0,
            0,
            0
        ]
    },
    "45.png": {
        "Placa_sistema": "HZS220",
        "Placa_usuario": "HZS220",
        "Pos_sistema": [
            {
                "x": 571,
                "y": 323
            },
            {
                "x": 702,
                "y": 326
            },
            {
                "x": 702,
                "y": 415
            },
            {
                "x": 571,
                "y": 412
            }
        ],
        "Pos_usuario": [
            569,
            337,
            138,
            86
        ]
    },
    "46.png": {
        "Placa_sistema": "BYQ037",
        "Placa_usuario": "BYQ037",
        "Pos_sistema": [
            {
                "x": 537,
                "y": 298
            },
            {
                "x": 654,
                "y": 302
            },
            {
                "x": 654,
                "y": 356
            },
            {
                "x": 537,
                "y": 351
            }
        ],
        "Pos_usuario": [
            538,
            302,
            96,
            57
        ]
    },
    "47.png": {
        "Placa_sistema": "BYQ037",
        "Placa_usuario": "BYQ037",
        "Pos_sistema": [
            {
                "x": 429,
                "y": 302
            },
            {
                "x": 522,
                "y": 304
            },
            {
                "x": 521,
                "y": 362
            },
            {
                "x": 429,
                "y": 359
            }
        ],
        "Pos_usuario": [
            429,
            303,
            100,
            67
        ]
    },
    "48.png": {
        "Placa_sistema": 0,
        "Placas_usuario": "DEP123",
        "Pos_sistema": [
            {
                "x": 84,
                "y": 235
            },
            {
                "x": 215,
                "y": 235
            },
            {
                "x": 215,
                "y": 281
            },
            {
                "x": 84,
                "y": 281
            }
        ],
        "Pos_usuario": [
            606,
            295,
            101,
            59
        ]
    },
    "49 (1).png": {
        "Placa_sistema": "DLS119",
        "Placa_usuario": "DLS119",
        "Pos_sistema": [
            {
                "x": 688,
                "y": 272
            },
            {
                "x": 789,
                "y": 272
            },
            {
                "x": 789,
                "y": 325
            },
            {
                "x": 688,
                "y": 325
            }
        ],
        "Pos_usuario": [
            703,
            283,
            89,
            47
        ]
    },
    "49.png": {
        "Placa_sistema": "DLS119",
        "Placa_usuario": "DLS119",
        "Pos_sistema": [
            {
                "x": 688,
                "y": 272
            },
            {
                "x": 789,
                "y": 272
            },
            {
                "x": 789,
                "y": 325
            },
            {
                "x": 688,
                "y": 325
            }
        ],
        "Pos_usuario": [
            702,
            279,
            90,
            52
        ]
    },
    "5.png": {
        "Placa_sistema": 0,
        "Placa_usuario": "CUP480",
        "Pos_sistema": 0,
        "Pos_usuario": [
            430,
            304,
            108,
            71
        ]
    },
    "50.png": {
        "Placa_sistema": "DLS119",
        "Placa_usuario": "DLS119",
        "Pos_sistema": [
            {
                "x": 571,
                "y": 283
            },
            {
                "x": 683,
                "y": 283
            },
            {
                "x": 683,
                "y": 335
            },
            {
                "x": 571,
                "y": 335
            }
        ],
        "Pos_usuario": [
            589,
            287,
            98,
            56
        ]
    },
    "51.png": {
        "Placa_sistema": "VCS051",
        "Placa_usuario": "VCS051",
        "Pos_sistema": [
            {
                "x": 760,
                "y": 294
            },
            {
                "x": 846,
                "y": 294
            },
            {
                "x": 846,
                "y": 349
            },
            {
                "x": 760,
                "y": 349
            }
        ],
        "Pos_usuario": [
            759,
            300,
            94,
            52
        ]
    },
    "52.png": {
        "Placa_sistema": 0,
        "Placa_usuario": "CKA159",
        "Pos_sistema": 0,
        "Pos_usuario": [
            453,
            358,
            138,
            83
        ]
    },
    "53.png": {
        "Placa_sistema": 0,
        "Placa_usuario": "0",
        "Pos_sistema": 0,
        "Pos_usuario": [
            0,
            0,
            0,
            0
        ]
    },
    "54.png": {
        "Placa_sistema": 0,
        "Placa_usuario": "CQG194",
        "Pos_sistema": 0,
        "Pos_usuario": [
            291,
            317,
            105,
            83
        ]
    },
    "55 (1).png": {
        "Placa_sistema": 0,
        "Placa_usuario": "0",
        "Pos_sistema": 0,
        "Pos_usuario": [
            520,
            312,
            106,
            78
        ]
    },
    "55.png": {
        "Placa_sistema": 0,
        "Placa_usuario": "0",
        "Pos_sistema": 0,
        "Pos_usuario": [
            516,
            315,
            109,
            75
        ]
    },
    "56.png": {
        "Placa_sistema": 0,
        "Placas_usuario": "HMD758",
        "Pos_sistema": [
            {
                "x": 847,
                "y": 285
            },
            {
                "x": 921,
                "y": 285
            },
            {
                "x": 921,
                "y": 320
            },
            {
                "x": 848,
                "y": 320
            }
        ],
        "Pos_usuario": [
            846,
            283,
            79,
            42
        ]
    },
    "57 (1).png": {
        "Placa_sistema": 0,
        "Placas_usuario": "HMO758",
        "Pos_sistema": [
            {
                "x": 778,
                "y": 284
            },
            {
                "x": 868,
                "y": 284
            },
            {
                "x": 868,
                "y": 335
            },
            {
                "x": 778,
                "y": 335
            }
        ],
        "Pos_usuario": [
            790,
            284,
            81,
            48
        ]
    },
    "57.png": {
        "Placa_sistema": 0,
        "Placas_usuario": "HMO758",
        "Pos_sistema": [
            {
                "x": 778,
                "y": 284
            },
            {
                "x": 868,
                "y": 284
            },
            {
                "x": 868,
                "y": 335
            },
            {
                "x": 778,
                "y": 335
            }
        ],
        "Pos_usuario": [
            788,
            286,
            84,
            45
        ]
    },
    "58.png": {
        "Placa_sistema": 0,
        "Placas_usuario": "NAK146",
        "Pos_sistema": [
            {
                "x": 515,
                "y": 300
            },
            {
                "x": 654,
                "y": 303
            },
            {
                "x": 654,
                "y": 382
            },
            {
                "x": 515,
                "y": 379
            }
        ],
        "Pos_usuario": [
            538,
            303,
            116,
            71
        ]
    },
    "59.png": {
        "Placa_sistema": "MAN716",
        "Placa_usuario": "MAN716",
        "Pos_sistema": [
            {
                "x": 727,
                "y": 324
            },
            {
                "x": 831,
                "y": 326
            },
            {
                "x": 831,
                "y": 374
            },
            {
                "x": 727,
                "y": 369
            }
        ],
        "Pos_usuario": [
            738,
            325,
            102,
            54
        ]
    },
    "6.png": {
        "Placa_sistema": "VCB153",
        "Placa_usuario": "VCB153",
        "Pos_sistema": [
            {
                "x": 803,
                "y": 253
            },
            {
                "x": 886,
                "y": 251
            },
            {
                "x": 885,
                "y": 293
            },
            {
                "x": 802,
                "y": 294
            }
        ],
        "Pos_usuario": [
            801,
            253,
            84,
            43
        ]
    },
    "60.png": {
        "Placa_sistema": 0,
        "Placas_usuario": "CWS662",
        "Pos_sistema": [
            {
                "x": 668,
                "y": 221
            },
            {
                "x": 776,
                "y": 219
            },
            {
                "x": 778,
                "y": 275
            },
            {
                "x": 670,
                "y": 277
            }
        ],
        "Pos_usuario": [
            685,
            229,
            93,
            55
        ]
    },
    "61.png": {
        "Placa_sistema": 0,
        "Placa_usuario": "CEF155",
        "Pos_sistema": 0,
        "Pos_usuario": [
            407,
            339,
            116,
            83
        ]
    },
    "62.png": {
        "Placa_sistema": 0,
        "Placas_usuario": "CBF152",
        "Pos_sistema": [
            {
                "x": 129,
                "y": 225
            },
            {
                "x": 208,
                "y": 227
            },
            {
                "x": 206,
                "y": 284
            },
            {
                "x": 133,
                "y": 283
            }
        ],
        "Pos_usuario": [
            642,
            282,
            115,
            58
        ]
    },
    "63.png": {
        "Placa_sistema": 0,
        "Placa_usuario": "0",
        "Pos_sistema": 0,
        "Pos_usuario": [
            337,
            300,
            105,
            85
        ]
    },
    "64.png": {
        "Placa_sistema": 0,
        "Placa_usuario": "0",
        "Pos_sistema": 0,
        "Pos_usuario": [
            363,
            235,
            126,
            81
        ]
    },
    "65.png": {
        "Placa_sistema": 0,
        "Placa_usuario": "CPE776",
        "Pos_sistema": 0,
        "Pos_usuario": [
            279,
            297,
            136,
            86
        ]
    },
    "66.png": {
        "Placa_sistema": "CPZ034",
        "Placa_usuario": "CPZ034",
        "Pos_sistema": [
            {
                "x": 585,
                "y": 319
            },
            {
                "x": 700,
                "y": 323
            },
            {
                "x": 700,
                "y": 377
            },
            {
                "x": 585,
                "y": 372
            }
        ],
        "Pos_usuario": [
            611,
            324,
            97,
            63
        ]
    },
    "67.png": {
        "Placa_sistema": 0,
        "Placa_usuario": "CQC934",
        "Pos_sistema": 0,
        "Pos_usuario": [
            494,
            309,
            95,
            64
        ]
    },
    "68.png": {
        "Placa_sistema": 0,
        "Placas_usuario": "VCW453",
        "Pos_sistema": [
            {
                "x": 761,
                "y": 268
            },
            {
                "x": 834,
                "y": 264
            },
            {
                "x": 837,
                "y": 303
            },
            {
                "x": 762,
                "y": 304
            }
        ],
        "Pos_usuario": [
            762,
            269,
            77,
            38
        ]
    },
    "69.png": {
        "Placa_sistema": 0,
        "Placa_usuario": "0",
        "Pos_sistema": 0,
        "Pos_usuario": [
            572,
            255,
            107,
            63
        ]
    },
    "7 (1).png": {
        "Placa_sistema": 0,
        "Placa_usuario": "0",
        "Pos_sistema": 0,
        "Pos_usuario": [
            0,
            0,
            0,
            0
        ]
    },
    "7.png": {
        "Placa_sistema": 0,
        "Placa_usuario": "0",
        "Pos_sistema": 0,
        "Pos_usuario": [
            0,
            0,
            0,
            0
        ]
    },
    "70.png": {
        "Placa_sistema": 0,
        "Placas_usuario": "HEL193",
        "Pos_sistema": [
            {
                "x": 511,
                "y": 239
            },
            {
                "x": 606,
                "y": 234
            },
            {
                "x": 606,
                "y": 293
            },
            {
                "x": 511,
                "y": 295
            }
        ],
        "Pos_usuario": [
            509,
            239,
            102,
            62
        ]
    },
    "71.png": {
        "Placa_sistema": 0,
        "Placas_usuario": "HEL193",
        "Pos_sistema": [
            {
                "x": 362,
                "y": 226
            },
            {
                "x": 461,
                "y": 220
            },
            {
                "x": 463,
                "y": 292
            },
            {
                "x": 362,
                "y": 299
            }
        ],
        "Pos_usuario": [
            354,
            229,
            115,
            76
        ]
    },
    "72.png": {
        "Placa_sistema": 0,
        "Placa_usuario": "0",
        "Pos_sistema": 0,
        "Pos_usuario": [
            527,
            360,
            100,
            67
        ]
    },
    "73.png": {
        "Placa_sistema": 0,
        "Placa_usuario": "WHD393",
        "Pos_sistema": 0,
        "Pos_usuario": [
            605,
            284,
            82,
            47
        ]
    },
    "74.png": {
        "Placa_sistema": 0,
        "Placa_usuario": "0",
        "Pos_sistema": 0,
        "Pos_usuario": [
            444,
            323,
            126,
            75
        ]
    },
    "75.png": {
        "Placa_sistema": 0,
        "Placa_usuario": "VCV229",
        "Pos_sistema": 0,
        "Pos_usuario": [
            400,
            287,
            101,
            68
        ]
    },
    "76.png": {
        "Placa_sistema": 0,
        "Placa_usuario": "BJG304",
        "Pos_sistema": 0,
        "Pos_usuario": [
            622,
            305,
            109,
            71
        ]
    },
    "77.png": {
        "Placa_sistema": 0,
        "Placa_usuario": "0",
        "Pos_sistema": 0,
        "Pos_usuario": [
            492,
            305,
            106,
            65
        ]
    },
    "78.png": {
        "Placa_sistema": 0,
        "Placas_usuario": "CUP552",
        "Pos_sistema": [
            {
                "x": 687,
                "y": 306
            },
            {
                "x": 786,
                "y": 295
            },
            {
                "x": 789,
                "y": 343
            },
            {
                "x": 691,
                "y": 355
            }
        ],
        "Pos_usuario": [
            699,
            295,
            96,
            55
        ]
    },
    "79.png": {
        "Placa_sistema": 0,
        "Placa_usuario": "BHG593",
        "Pos_sistema": 0,
        "Pos_usuario": [
            614,
            268,
            108,
            62
        ]
    },
    "8.png": {
        "Placa_sistema": 0,
        "Placas_usuario": "FCX257",
        "Pos_sistema": [
            {
                "x": 165,
                "y": 91
            },
            {
                "x": 280,
                "y": 94
            },
            {
                "x": 283,
                "y": 176
            },
            {
                "x": 167,
                "y": 173
            }
        ],
        "Pos_usuario": [
            754,
            301,
            96,
            57
        ]
    },
    "80.png": {
        "Placa_sistema": 0,
        "Placas_usuario": "MTM926",
        "Pos_sistema": [
            {
                "x": 591,
                "y": 295
            },
            {
                "x": 701,
                "y": 299
            },
            {
                "x": 701,
                "y": 356
            },
            {
                "x": 591,
                "y": 352
            }
        ],
        "Pos_usuario": [
            616,
            307,
            93,
            54
        ]
    },
    "81.png": {
        "Placa_sistema": "CQE810",
        "Placa_usuario": "CQE810",
        "Pos_sistema": [
            {
                "x": 250,
                "y": 326
            },
            {
                "x": 396,
                "y": 338
            },
            {
                "x": 396,
                "y": 410
            },
            {
                "x": 250,
                "y": 398
            }
        ],
        "Pos_usuario": [
            293,
            336,
            107,
            83
        ]
    },
    "82.png": {
        "Placa_sistema": 0,
        "Placas_usuario": "TZN906",
        "Pos_sistema": [
            {
                "x": 677,
                "y": 282
            },
            {
                "x": 774,
                "y": 280
            },
            {
                "x": 773,
                "y": 342
            },
            {
                "x": 679,
                "y": 344
            }
        ],
        "Pos_usuario": [
            678,
            280,
            102,
            59
        ]
    },
    "83.png": {
        "Placa_sistema": 0,
        "Placa_usuario": "HPR581",
        "Pos_sistema": 0,
        "Pos_usuario": [
            566,
            299,
            103,
            67
        ]
    },
    "84.png": {
        "Placa_sistema": 0,
        "Placa_usuario": "MTM978",
        "Pos_sistema": 0,
        "Pos_usuario": [
            498,
            279,
            121,
            78
        ]
    },
    "85 (1).png": {
        "Placa_sistema": 0,
        "Placa_usuario": "0",
        "Pos_sistema": 0,
        "Pos_usuario": [
            447,
            298,
            100,
            66
        ]
    },
    "85.png": {
        "Placa_sistema": 0,
        "Placa_usuario": "0",
        "Pos_sistema": 0,
        "Pos_usuario": [
            446,
            298,
            101,
            62
        ]
    },
    "86.png": {
        "Placa_sistema": 0,
        "Placa_usuario": "0",
        "Pos_sistema": 0,
        "Pos_usuario": [
            543,
            331,
            125,
            75
        ]
    },
    "87.png": {
        "Placa_sistema": 0,
        "Placas_usuario": "UBR567",
        "Pos_sistema": [
            {
                "x": 50,
                "y": 96
            },
            {
                "x": 237,
                "y": 93
            },
            {
                "x": 238,
                "y": 186
            },
            {
                "x": 51,
                "y": 189
            }
        ],
        "Pos_usuario": [
            670,
            293,
            115,
            63
        ]
    },
    "88.png": {
        "Placa_sistema": 0,
        "Placas_usuario": "KIP666",
        "Pos_sistema": [
            {
                "x": 620,
                "y": 198
            },
            {
                "x": 742,
                "y": 195
            },
            {
                "x": 744,
                "y": 261
            },
            {
                "x": 622,
                "y": 262
            }
        ],
        "Pos_usuario": [
            626,
            195,
            121,
            71
        ]
    },
    "89.png": {
        "Placa_sistema": 0,
        "Placas_usuario": "KIO028",
        "Pos_sistema": [
            {
                "x": 625,
                "y": 296
            },
            {
                "x": 730,
                "y": 299
            },
            {
                "x": 730,
                "y": 352
            },
            {
                "x": 625,
                "y": 348
            }
        ],
        "Pos_usuario": [
            625,
            296,
            109,
            62
        ]
    },
    "9.png": {
        "Placa_sistema": 0,
        "Placa_usuario": "FCX257",
        "Pos_sistema": 0,
        "Pos_usuario": [
            516,
            315,
            116,
            83
        ]
    },
    "90.png": {
        "Placa_sistema": 0,
        "Placas_usuario": "CPR130",
        "Pos_sistema": [
            {
                "x": 652,
                "y": 324
            },
            {
                "x": 803,
                "y": 332
            },
            {
                "x": 800,
                "y": 409
            },
            {
                "x": 649,
                "y": 401
            }
        ],
        "Pos_usuario": [
            675,
            335,
            136,
            84
        ]
    }
    
}