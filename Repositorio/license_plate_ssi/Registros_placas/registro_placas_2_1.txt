{"1.png": {
        "Placa_sistema": 0,
        "Placas_usuario": "BRA504",
        "Pos_sistema": [
            {
                "x": 466,
                "y": 258
            },
            {
                "x": 649,
                "y": 259
            },
            {
                "x": 649,
                "y": 347
            },
            {
                "x": 466,
                "y": 344
            }
        ],
        "Pos_usuario": [
            493,
            265,
            132,
            93
        ]
    },
    "10.png": {
        "Placa_sistema": 0,
        "Placa_usuario": "UTM948",
        "Pos_sistema": 0,
        "Pos_usuario": [
            949,
            214,
            89,
            44
        ]
    },
    "11.png": {
        "Placa_sistema": "IIT188",
        "Placa_usuario": "IIT188",
        "Pos_sistema": [
            {
                "x": 898,
                "y": 258
            },
            {
                "x": 1009,
                "y": 263
            },
            {
                "x": 1006,
                "y": 318
            },
            {
                "x": 895,
                "y": 313
            }
        ],
        "Pos_usuario": [
            895,
            262,
            112,
            60
        ]
    },
    "12.png": {
        "Placa_sistema": "IIT188",
        "Placa_usuario": "IIT188",
        "Pos_sistema": [
            {
                "x": 629,
                "y": 271
            },
            {
                "x": 747,
                "y": 276
            },
            {
                "x": 743,
                "y": 356
            },
            {
                "x": 626,
                "y": 351
            }
        ],
        "Pos_usuario": [
            623,
            283,
            133,
            84
        ]
    },
    "13.png": {
        "Placa_sistema": 0,
        "Placa_usuario": "CMC113",
        "Pos_sistema": 0,
        "Pos_usuario": [
            503,
            229,
            119,
            76
        ]
    },
    "14.png": {
        "Placa_sistema": 0,
        "Placa_usuario": "CMC113",
        "Pos_sistema": 0,
        "Pos_usuario": [
            454,
            221,
            113,
            78
        ]
    },
    "15.png": {
        "Placa_sistema": 0,
        "Placa_usuario": "KCC737",
        "Pos_sistema": 0,
        "Pos_usuario": [
            458,
            159,
            81,
            60
        ]
    },
    "16.png": {
        "Placa_sistema": 0,
        "Placa_usuario": "KCC737",
        "Pos_sistema": 0,
        "Pos_usuario": [
            238,
            154,
            67,
            66
        ]
    },
    "17.png": {
        "Placa_sistema": 0,
        "Placas_usuario": "VCT398",
        "Pos_sistema": [
            {
                "x": 432,
                "y": 257
            },
            {
                "x": 539,
                "y": 257
            },
            {
                "x": 541,
                "y": 340
            },
            {
                "x": 434,
                "y": 340
            }
        ],
        "Pos_usuario": [
            434,
            265,
            109,
            81
        ]
    },
    "18.png": {
        "Placa_sistema": 0,
        "Placas_usuario": "VCT398",
        "Pos_sistema": [
            {
                "x": 429,
                "y": 257
            },
            {
                "x": 536,
                "y": 257
            },
            {
                "x": 537,
                "y": 339
            },
            {
                "x": 429,
                "y": 339
            }
        ],
        "Pos_usuario": [
            428,
            266,
            113,
            80
        ]
    },
    "19.png": {
        "Placa_sistema": "HYM168",
        "Placa_usuario": "HYM168",
        "Pos_sistema": [
            {
                "x": 649,
                "y": 218
            },
            {
                "x": 759,
                "y": 214
            },
            {
                "x": 761,
                "y": 286
            },
            {
                "x": 651,
                "y": 291
            }
        ],
        "Pos_usuario": [
            651,
            219,
            109,
            71
        ]
    },
    "2.png": {
        "Placa_sistema": "KDP856",
        "Placa_usuario": "KDP856",
        "Pos_sistema": [
            {
                "x": 665,
                "y": 237
            },
            {
                "x": 817,
                "y": 237
            },
            {
                "x": 817,
                "y": 317
            },
            {
                "x": 665,
                "y": 317
            }
        ],
        "Pos_usuario": [
            691,
            237,
            130,
            73
        ]
    },
    "20.png": {
        "Placa_sistema": 0,
        "Placa_usuario": "VCT336",
        "Pos_sistema": 0,
        "Pos_usuario": [
            624,
            244,
            113,
            71
        ]
    },
    "21.png": {
        "Placa_sistema": 0,
        "Placas_usuario": "BOV940",
        "Pos_sistema": [
            {
                "x": 742,
                "y": 233
            },
            {
                "x": 881,
                "y": 236
            },
            {
                "x": 881,
                "y": 306
            },
            {
                "x": 742,
                "y": 303
            }
        ],
        "Pos_usuario": [
            743,
            235,
            123,
            73
        ]
    },
    "22.png": {
        "Placa_sistema": "BOV940",
        "Placa_usuario": "BOV940",
        "Pos_sistema": [
            {
                "x": 721,
                "y": 224
            },
            {
                "x": 841,
                "y": 224
            },
            {
                "x": 841,
                "y": 302
            },
            {
                "x": 720,
                "y": 302
            }
        ],
        "Pos_usuario": [
            722,
            233,
            124,
            73
        ]
    },
    "23.png": {
        "Placa_sistema": 0,
        "Placas_usuario": "TJW544",
        "Pos_sistema": [
            {
                "x": 622,
                "y": 244
            },
            {
                "x": 739,
                "y": 239
            },
            {
                "x": 752,
                "y": 294
            },
            {
                "x": 634,
                "y": 299
            }
        ],
        "Pos_usuario": [
            635,
            241,
            120,
            59
        ]
    },
    "24.png": {
        "Placa_sistema": 0,
        "Placas_usuario": "CAX576",
        "Pos_sistema": [
            {
                "x": 304,
                "y": 277
            },
            {
                "x": 482,
                "y": 296
            },
            {
                "x": 478,
                "y": 383
            },
            {
                "x": 299,
                "y": 363
            }
        ],
        "Pos_usuario": [
            372,
            295,
            113,
            97
        ]
    },
    "25.png": {
        "Placa_sistema": "CAT816",
        "Placa_usuario": "CAT816",
        "Pos_sistema": [
            {
                "x": 719,
                "y": 262
            },
            {
                "x": 858,
                "y": 264
            },
            {
                "x": 858,
                "y": 335
            },
            {
                "x": 719,
                "y": 333
            }
        ],
        "Pos_usuario": [
            728,
            271,
            123,
            75
        ]
    },
    "26.png": {
        "Placa_sistema": 0,
        "Placa_usuario": "VCZ170",
        "Pos_sistema": 0,
        "Pos_usuario": [
            460,
            255,
            127,
            91
        ]
    },
    "27.png": {
        "Placa_sistema": 0,
        "Placa_usuario": "HZU755",
        "Pos_sistema": 0,
        "Pos_usuario": [
            958,
            246,
            92,
            50
        ]
    },
    "28.png": {
        "Placa_sistema": 0,
        "Placa_usuario": "HZU755",
        "Pos_sistema": 0,
        "Pos_usuario": [
            313,
            296,
            120,
            109
        ]
    },
    "29.png": {
        "Placa_sistema": 0,
        "Placa_usuario": "CUR562",
        "Pos_sistema": 0,
        "Pos_usuario": [
            437,
            249,
            118,
            87
        ]
    },
    "3.png": {
        "Placa_sistema": 0,
        "Placa_usuario": "KGY458",
        "Pos_sistema": 0,
        "Pos_usuario": [
            571,
            273,
            131,
            91
        ]
    },
    "30.png": {
        "Placa_sistema": 0,
        "Placa_usuario": "CQH540",
        "Pos_sistema": 0,
        "Pos_usuario": [
            309,
            262,
            131,
            108
        ]
    },
    "31.png": {
        "Placa_sistema": 0,
        "Placa_usuario": "CAV775",
        "Pos_sistema": 0,
        "Pos_usuario": [
            439,
            273,
            127,
            102
        ]
    },
    "32.png": {
        "Placa_sistema": 0,
        "Placa_usuario": "0",
        "Pos_sistema": 0,
        "Pos_usuario": [
            1086,
            220,
            84,
            44
        ]
    },
    "33.png": {
        "Placa_sistema": "ONK384",
        "Placa_usuario": "ONK384",
        "Pos_sistema": [
            {
                "x": 574,
                "y": 230
            },
            {
                "x": 693,
                "y": 232
            },
            {
                "x": 695,
                "y": 311
            },
            {
                "x": 574,
                "y": 309
            }
        ],
        "Pos_usuario": [
            571,
            235,
            126,
            86
        ]
    },
    "34.png": {
        "Placa_sistema": 0,
        "Placa_usuario": "VCM505",
        "Pos_sistema": 0,
        "Pos_usuario": [
            1145,
            229,
            81,
            35
        ]
    },
    "35.png": {
        "Placa_sistema": "VCM505",
        "Placa_usuario": "VCM505",
        "Pos_sistema": [
            {
                "x": 536,
                "y": 251
            },
            {
                "x": 648,
                "y": 254
            },
            {
                "x": 648,
                "y": 334
            },
            {
                "x": 537,
                "y": 331
            }
        ],
        "Pos_usuario": [
            532,
            247,
            121,
            86
        ]
    },
    "36.png": {
        "Placa_sistema": 0,
        "Placa_usuario": "UGR965",
        "Pos_sistema": 0,
        "Pos_usuario": [
            1034,
            235,
            81,
            40
        ]
    },
    "37.png": {
        "Placa_sistema": 0,
        "Placas_usuario": "UGR965",
        "Pos_sistema": [
            {
                "x": 575,
                "y": 251
            },
            {
                "x": 674,
                "y": 254
            },
            {
                "x": 674,
                "y": 332
            },
            {
                "x": 570,
                "y": 328
            }
        ],
        "Pos_usuario": [
            564,
            253,
            123,
            79
        ]
    },
    "38.png": {
        "Placa_sistema": 0,
        "Placas_usuario": "IIP896",
        "Pos_sistema": [
            {
                "x": 1067,
                "y": 232
            },
            {
                "x": 1144,
                "y": 230
            },
            {
                "x": 1146,
                "y": 268
            },
            {
                "x": 1069,
                "y": 270
            }
        ],
        "Pos_usuario": [
            1068,
            234,
            78,
            36
        ]
    },
    "39.png": {
        "Placa_sistema": "IIP896",
        "Placa_usuario": "IIP896",
        "Pos_sistema": [
            {
                "x": 461,
                "y": 261
            },
            {
                "x": 577,
                "y": 261
            },
            {
                "x": 577,
                "y": 354
            },
            {
                "x": 461,
                "y": 354
            }
        ],
        "Pos_usuario": [
            459,
            262,
            123,
            84
        ]
    },
    "4.png": {
        "Placa_sistema": 0,
        "Placas_usuario": "HML909",
        "Pos_sistema": [
            {
                "x": 623,
                "y": 248
            },
            {
                "x": 746,
                "y": 250
            },
            {
                "x": 746,
                "y": 312
            },
            {
                "x": 623,
                "y": 310
            }
        ],
        "Pos_usuario": [
            648,
            251,
            101,
            64
        ]
    },
    "40.png": {
        "Placa_sistema": 0,
        "Placa_usuario": "0",
        "Pos_sistema": 0,
        "Pos_usuario": [
            0,
            0,
            0,
            0
        ]
    },
    "41.png": {
        "Placa_sistema": 0,
        "Placas_usuario": "IFX539",
        "Pos_sistema": [
            {
                "x": 979,
                "y": 244
            },
            {
                "x": 1069,
                "y": 246
            },
            {
                "x": 1071,
                "y": 292
            },
            {
                "x": 981,
                "y": 291
            }
        ],
        "Pos_usuario": [
            981,
            252,
            94,
            52
        ]
    },
    "42.png": {
        "Placa_sistema": "IFX539",
        "Placa_usuario": "IFX539",
        "Pos_sistema": [
            {
                "x": 678,
                "y": 256
            },
            {
                "x": 825,
                "y": 261
            },
            {
                "x": 822,
                "y": 338
            },
            {
                "x": 675,
                "y": 333
            }
        ],
        "Pos_usuario": [
            676,
            270,
            116,
            77
        ]
    },
    "43.png": {
        "Placa_sistema": 0,
        "Placa_usuario": "0",
        "Pos_sistema": 0,
        "Pos_usuario": [
            0,
            0,
            0,
            0
        ]
    },
    "44.png": {
        "Placa_sistema": 0,
        "Placa_usuario": "CPS029",
        "Pos_sistema": 0,
        "Pos_usuario": [
            276,
            291,
            130,
            110
        ]
    },
    "45.png": {
        "Placa_sistema": 0,
        "Placas_usuario": "TZO036",
        "Pos_sistema": [
            {
                "x": 936,
                "y": 237
            },
            {
                "x": 1016,
                "y": 240
            },
            {
                "x": 1016,
                "y": 279
            },
            {
                "x": 936,
                "y": 277
            }
        ],
        "Pos_usuario": [
            931,
            240,
            81,
            50
        ]
    },
    "46.png": {
        "Placa_sistema": "TZO036",
        "Placa_usuario": "TZO036",
        "Pos_sistema": [
            {
                "x": 640,
                "y": 255
            },
            {
                "x": 734,
                "y": 258
            },
            {
                "x": 734,
                "y": 320
            },
            {
                "x": 640,
                "y": 315
            }
        ],
        "Pos_usuario": [
            636,
            256,
            102,
            65
        ]
    },
    "47.png": {
        "Placa_sistema": 0,
        "Placa_usuario": "0",
        "Pos_sistema": 0,
        "Pos_usuario": [
            0,
            0,
            0,
            0
        ]
    },
    "48.png": {
        "Placa_sistema": 0,
        "Placas_usuario": "HMP721",
        "Pos_sistema": [
            {
                "x": 415,
                "y": 221
            },
            {
                "x": 522,
                "y": 221
            },
            {
                "x": 522,
                "y": 318
            },
            {
                "x": 415,
                "y": 318
            }
        ],
        "Pos_usuario": [
            409,
            231,
            117,
            87
        ]
    },
    "49.png": {
        "Placa_sistema": 0,
        "Placa_usuario": "KGZ222",
        "Pos_sistema": 0,
        "Pos_usuario": [
            1100,
            231,
            79,
            39
        ]
    },
    "5.png": {
        "Placa_sistema": 0,
        "Placa_usuario": "CQJ233",
        "Pos_sistema": 0,
        "Pos_usuario": [
            1111,
            231,
            76,
            39
        ]
    },
    "50 (1).png": {
        "Placa_sistema": 0,
        "Placa_usuario": "KGZ222",
        "Pos_sistema": 0,
        "Pos_usuario": [
            591,
            258,
            118,
            79
        ]
    },
    "50.png": {
        "Placa_sistema": 0,
        "Placa_usuario": "KGZ222",
        "Pos_sistema": 0,
        "Pos_usuario": [
            590,
            257,
            115,
            79
        ]
    },
    "51.png": {
        "Placa_sistema": "CPT110",
        "Placa_usuario": "CPT110",
        "Pos_sistema": [
            {
                "x": 970,
                "y": 237
            },
            {
                "x": 1063,
                "y": 237
            },
            {
                "x": 1063,
                "y": 287
            },
            {
                "x": 968,
                "y": 287
            }
        ],
        "Pos_usuario": [
            964,
            238,
            102,
            48
        ]
    },
    "52.png": {
        "Placa_sistema": "CPT110",
        "Placa_usuario": "CPT110",
        "Pos_sistema": [
            {
                "x": 468,
                "y": 256
            },
            {
                "x": 598,
                "y": 264
            },
            {
                "x": 592,
                "y": 340
            },
            {
                "x": 464,
                "y": 335
            }
        ],
        "Pos_usuario": [
            470,
            260,
            124,
            88
        ]
    },
    "53.png": {
        "Placa_sistema": "DIR786",
        "Placa_usuario": "DIR786",
        "Pos_sistema": [
            {
                "x": 867,
                "y": 227
            },
            {
                "x": 959,
                "y": 228
            },
            {
                "x": 958,
                "y": 280
            },
            {
                "x": 866,
                "y": 279
            }
        ],
        "Pos_usuario": [
            882,
            242,
            84,
            44
        ]
    },
    "54.png": {
        "Placa_sistema": 0,
        "Placas_usuario": "DIR786",
        "Pos_sistema": [
            {
                "x": 363,
                "y": 261
            },
            {
                "x": 529,
                "y": 267
            },
            {
                "x": 529,
                "y": 352
            },
            {
                "x": 363,
                "y": 347
            }
        ],
        "Pos_usuario": [
            397,
            259,
            105,
            83
        ]
    },
    "55.png": {
        "Placa_sistema": 0,
        "Placa_usuario": "HPW355",
        "Pos_sistema": 0,
        "Pos_usuario": [
            1057,
            227,
            72,
            39
        ]
    },
    "56.png": {
        "Placa_sistema": 0,
        "Placas_usuario": "HPM355",
        "Pos_sistema": [
            {
                "x": 482,
                "y": 238
            },
            {
                "x": 625,
                "y": 241
            },
            {
                "x": 626,
                "y": 310
            },
            {
                "x": 484,
                "y": 308
            }
        ],
        "Pos_usuario": [
            526,
            243,
            105,
            77
        ]
    },
    "57.png": {
        "Placa_sistema": "MHN248",
        "Placa_usuario": "MHN248",
        "Pos_sistema": [
            {
                "x": 760,
                "y": 261
            },
            {
                "x": 919,
                "y": 265
            },
            {
                "x": 913,
                "y": 341
            },
            {
                "x": 756,
                "y": 336
            }
        ],
        "Pos_usuario": [
            760,
            269,
            130,
            76
        ]
    },
    "58.png": {
        "Placa_sistema": 0,
        "Placa_usuario": "MHN248",
        "Pos_sistema": 0,
        "Pos_usuario": [
            348,
            291,
            137,
            120
        ]
    },
    "59.png": {
        "Placa_sistema": "IIR669",
        "Placa_usuario": "IIR669",
        "Pos_sistema": [
            {
                "x": 585,
                "y": 217
            },
            {
                "x": 682,
                "y": 220
            },
            {
                "x": 681,
                "y": 295
            },
            {
                "x": 580,
                "y": 292
            }
        ],
        "Pos_usuario": [
            578,
            222,
            113,
            73
        ]
    },
    "6.png": {
        "Placa_sistema": 0,
        "Placas_usuario": "CQJ233",
        "Pos_sistema": [
            {
                "x": 460,
                "y": 253
            },
            {
                "x": 651,
                "y": 260
            },
            {
                "x": 649,
                "y": 361
            },
            {
                "x": 458,
                "y": 354
            }
        ],
        "Pos_usuario": [
            506,
            265,
            149,
            106
        ]
    },
    "60.png": {
        "Placa_sistema": 0,
        "Placas_usuario": "CUL257",
        "Pos_sistema": [
            {
                "x": 424,
                "y": 275
            },
            {
                "x": 561,
                "y": 279
            },
            {
                "x": 561,
                "y": 348
            },
            {
                "x": 424,
                "y": 344
            }
        ],
        "Pos_usuario": [
            463,
            276,
            103,
            65
        ]
    },
    "61.png": {
        "Placa_sistema": "QEQ510",
        "Placa_usuario": "QEQ510",
        "Pos_sistema": [
            {
                "x": 535,
                "y": 273
            },
            {
                "x": 670,
                "y": 282
            },
            {
                "x": 675,
                "y": 347
            },
            {
                "x": 540,
                "y": 339
            }
        ],
        "Pos_usuario": [
            575,
            284,
            109,
            67
        ]
    },
    "62.png": {
        "Placa_sistema": 0,
        "Placa_usuario": "UGN620",
        "Pos_sistema": 0,
        "Pos_usuario": [
            527,
            206,
            102,
            65
        ]
    },
    "63.png": {
        "Placa_sistema": 0,
        "Placa_usuario": "0",
        "Pos_sistema": 0,
        "Pos_usuario": [
            1063,
            186,
            66,
            29
        ]
    },
    "64.png": {
        "Placa_sistema": 0,
        "Placas_usuario": "KIN590",
        "Pos_sistema": [
            {
                "x": 381,
                "y": 250
            },
            {
                "x": 563,
                "y": 255
            },
            {
                "x": 560,
                "y": 349
            },
            {
                "x": 379,
                "y": 344
            }
        ],
        "Pos_usuario": [
            413,
            261,
            121,
            89
        ]
    },
    "65.png": {
        "Placa_sistema": "RLO360",
        "Placa_usuario": "RLO360",
        "Pos_sistema": [
            {
                "x": 627,
                "y": 266
            },
            {
                "x": 772,
                "y": 272
            },
            {
                "x": 769,
                "y": 342
            },
            {
                "x": 624,
                "y": 336
            }
        ],
        "Pos_usuario": [
            643,
            272,
            113,
            73
        ]
    },
    "66.png": {
        "Placa_sistema": 0,
        "Placa_usuario": "CLY155",
        "Pos_sistema": 0,
        "Pos_usuario": [
            509,
            305,
            149,
            97
        ]
    },
    "67 (1).png": {
        "Placa_sistema": 0,
        "Placas_usuario": "CQA082",
        "Pos_sistema": [
            {
                "x": 485,
                "y": 269
            },
            {
                "x": 674,
                "y": 280
            },
            {
                "x": 670,
                "y": 378
            },
            {
                "x": 481,
                "y": 366
            }
        ],
        "Pos_usuario": [
            534,
            274,
            143,
            101
        ]
    },
    "67.png": {
        "Placa_sistema": 0,
        "Placas_usuario": "CQA082",
        "Pos_sistema": [
            {
                "x": 485,
                "y": 269
            },
            {
                "x": 674,
                "y": 280
            },
            {
                "x": 670,
                "y": 378
            },
            {
                "x": 481,
                "y": 366
            }
        ],
        "Pos_usuario": [
            536,
            279,
            143,
            100
        ]
    },
    "68.png": {
        "Placa_sistema": 0,
        "Placa_usuario": "0",
        "Pos_sistema": 0,
        "Pos_usuario": [
            396,
            274,
            109,
            89
        ]
    },
    "69.png": {
        "Placa_sistema": 0,
        "Placas_usuario": "GUL977",
        "Pos_sistema": [
            {
                "x": 547,
                "y": 260
            },
            {
                "x": 650,
                "y": 264
            },
            {
                "x": 646,
                "y": 345
            },
            {
                "x": 540,
                "y": 342
            }
        ],
        "Pos_usuario": [
            544,
            259,
            115,
            86
        ]
    },
    "7.png": {
        "Placa_sistema": "DLS644",
        "Placa_usuario": "DLS644",
        "Pos_sistema": [
            {
                "x": 622,
                "y": 247
            },
            {
                "x": 731,
                "y": 247
            },
            {
                "x": 731,
                "y": 319
            },
            {
                "x": 619,
                "y": 319
            }
        ],
        "Pos_usuario": [
            619,
            249,
            111,
            69
        ]
    },
    "70.png": {
        "Placa_sistema": "SBL068",
        "Placa_usuario": "SBL068",
        "Pos_sistema": [
            {
                "x": 525,
                "y": 225
            },
            {
                "x": 621,
                "y": 225
            },
            {
                "x": 621,
                "y": 291
            },
            {
                "x": 525,
                "y": 291
            }
        ],
        "Pos_usuario": [
            521,
            237,
            104,
            66
        ]
    },
    "71.png": {
        "Placa_sistema": 0,
        "Placa_usuario": "HMN764",
        "Pos_sistema": 0,
        "Pos_usuario": [
            457,
            240,
            125,
            92
        ]
    },
    "72.png": {
        "Placa_sistema": "IIT880",
        "Placa_usuario": "IIT880",
        "Pos_sistema": [
            {
                "x": 752,
                "y": 230
            },
            {
                "x": 878,
                "y": 232
            },
            {
                "x": 878,
                "y": 284
            },
            {
                "x": 752,
                "y": 281
            }
        ],
        "Pos_usuario": [
            746,
            224,
            102,
            59
        ]
    },
    "8.png": {
        "Placa_sistema": 0,
        "Placa_usuario": "0",
        "Pos_sistema": 0,
        "Pos_usuario": [
            1008,
            242,
            76,
            33
        ]
    },
    "9.png": {
        "Placa_sistema": "CQH697",
        "Placa_usuario": "COH697",
        "Pos_sistema": [
            {
                "x": 596,
                "y": 262
            },
            {
                "x": 691,
                "y": 267
            },
            {
                "x": 688,
                "y": 332
            },
            {
                "x": 589,
                "y": 327
            }
        ],
        "Pos_usuario": [
            596,
            262,
            96,
            69
        ]
    }
    }