#!/usr/bin/env python
# -*- coding: utf-8 -*-

import sys
# from  PyQt4 import QtGui,QtCore
import numpy as np
import os
from vispy.app import Timer
import logging
import multiprocessing
from multiprocessing import Queue, Process
import time
import datetime
import cv2
from com import DataSending

class ThreadsController:
    """
    Clase que interconecta los hilos del sistema con la interfaz grafica
    Atributos:
            DevicesModel        : Conexion con el modelo del sistema 
            SoftwareModels        : Conexion con el modelo software del sistema
            timerinterval               : temporizador que generara una interrupcion cada cierto intervalo cuando la  pesa;a este siendo visualizada
    """
    DevicesModel = None
    SoftwareModels = None
    timer = None
    threads_J = []

    def __init__(self, DevicesModel, MyTable, SuperLogger, timerinterval =1/20, Prototipe=False):
        # se guarda el modelo hardaware, el modelo software y la vista en los atributos de las clases
        self.DevicesModel = DevicesModel
        
        self.MyTable = MyTable
        self.SuperLogger = SuperLogger

        logging.getLogger().addHandler(self.SuperLogger)
        # logging.getLogger().setLevel(logging.DEBUG)

        self.result_table_d = np.zeros(14)
        self.result_table_i = np.zeros(14)
        self.Init_Table()
        self.error_count = 0

        self.hilo_5_Recv = None
        # se  crea un reloj que permitira actualizar la adquisicion de datos de os sensores emulando una interupcion y se guarda el intervalo predefinido
        self.timerinterval = timerinterval
        self.timer = Timer(connect= self.timerevent)
        self.prototype_b = Prototipe
        self.laser_list = None
        self.platebuffer = {}
        self.comBuffer =[]

    def Init_Table(self):
        for x in range(0,len(self.result_table_d)):
            self.MyTable.changeValue(x,1,str(self.result_table_d[x]))
            self.MyTable.changeValue(x,2,str(self.result_table_i[x]))

    def timerevent(self,event):
    	# logging.debug("Tiempo")
        if self.hilo_5_Recv!= None:
            if (not self.hilo_5_Recv.empty()):
                self.platebuffer = PlateDetection(self.DevicesModel.Cameras,self.hilo_5_Recv,self.DevicesModel.LPR,self.platebuffer)
        #Se revisa el estado de los hilos
        if self.error_count<3:
            Error_vector = self.checkState()
            for State, index_error in Error_vector:
            	# Si algun hilo ha muerto, se reinicia
                if State == True:
                    self.Reset_Thread(index_error)
                else:
                    pass
        else:
            pass
        # Se lee el buffer de salida del hilo de clasificacion
        self.update_vehicle_result()

    def Reset_Thread(self, index):
        # Se reinicia el hilo que finalizo/presento error
        logging.error("ERROR: %s, para %s, en un segundo se intentara reconectar el proceso", self.threads_J[index].exitcode, self.threads_J[index].name)
        self.threads_J[index].terminate()
        self.threads_J[index].join()
        # time.sleep(0.5)
        # self.threads_J[index].start()

        try:
            if index==0:
                laser_type, laser_port =self.laser_list[0]
                if laser_type=="Hokuyo-utm30-lx":

                    self.prototype_b = False
                    self.laser_p = multiprocessing.Process(name='laser', target=laserobt, args=(self.DevicesModel.Hokuyo_Dic[laser_port], self.DevicesModel.Radar_Dic[str(radar_ports [0])], 
                        self.DevicesModel.Radar_Dic[str(self.radar_ports [1])], self.DevicesModel.Sample_C_Iz, self.DevicesModel.Sample_C_De, self.DevicesModel.Hokuyo_Dic[laser_port].angle_a0, 
                        self.hilo_1_Recv,self.hilo_5_Recv))
                else:
                    self.prototype_b = True
                    self.laser_p = multiprocessing.Process(name='laser', target=rplidarobt, args=(self.DevicesModel.Laser_Dic[laser_port],
                    self.DevicesModel.Sample_C_Iz, self.DevicesModel.Sample_C_De, self.DevicesModel.Laser_Dic[laser_port].angle_a0, 
                        self.hilo_1_Recv,))                

                self.threads_J[0] = self.laser_p
                self.threads_J[0].start()
            if index==1:
                self.Inicial_Cloud = multiprocessing.Process(name='inicial_cloud', target=CloudCreacionobt, args=(self.hilo_1_Recv, self.hilo_2_Recv, self.DevicesModel.Creation_C_IZQ,
                    self.DevicesModel.Creation_C_DER, self.DevicesModel.Creation_RPC_IZQ,self.DevicesModel.Creation_RPC_DER,self.prototype_b, ))
                self.threads_J[1] = self.Inicial_Cloud
            	self.threads_J[1].start()
            if index==2:
            	self.Cluster_Descriptor = multiprocessing.Process(name='cluster_descriptor', target=ClusterAndDecriptorCpp, args=(self.hilo_2_Recv, self.hilo_3_Recv, 33, 
            	    self.DevicesModel.Segmentador_C, self.DevicesModel.Descriptor_C, self.DevicesModel.PassCloudClasses,))
            	self.threads_J[2] = self.Cluster_Descriptor
            	self.threads_J[2].start()
            if index==3:
            	self.clasiffier = multiprocessing.Process(name='clasiffier', target=Clasificate, args=(self.hilo_3_Recv, self.hilo_4_Recv, self.DevicesModel.SVM_C,
                    self.prototype_b,))
            	self.threads_J[3] = self.clasiffier
            	self.threads_J[3].start()
            time.sleep(0.5)
            logging.info("Reiniciando el proceso: %s", self.threads_J[index].name)
        except Exception as e:
            self.error_count = self.error_count + 1
            if self.error_count<3:
                logging.warning("ERROR: %s, para %s, en un segundo se intentara reconectar el proceso", self.threads_J[index].exitcode, self.threads_J[index].name)
            else:
                logging.warning("ERROR CONSTANTE: %s, para %s, se necesita una intervencion humana", self.threads_J[index].exitcode, self.threads_J[index].name)
        else:
            pass
        finally:
            pass

    def checkState(self):
    	#se reivisa el estado de todos los hilos
        out_aux = []
        for j in self.threads_J:
			
			if not (j.is_alive()):
			    bolean_aux = True
			else:
			    bolean_aux = False
			index_aux = self.threads_J.index(j)
			# print j.is_alive(), index_aux
			out_aux.append([bolean_aux, index_aux])
        return out_aux
                
    # def update_Logger(self):
    #     "REvisando"
            

    def update_vehicle_result(self):
        # Se lee el buffer de salida del hilo de clasificacion
        if (not self.hilo_4_Recv.empty()):
            Result, NumWheels,label, Right = self.hilo_4_Recv.get()
            if Right:
                self.result_table_d[Result] = self.result_table_d[Result] + 1
                self.MyTable.changeValue(Result,2,str(self.result_table_d[Result]))
            else:
                self.result_table_i[Result] = self.result_table_i[Result] + 1
                self.MyTable.changeValue(Result,1,str(self.result_table_i[Result]))
            if label in self.platebuffer:
                plate = self.platebuffer[label]
                del self.platebuffer[label]
            else:
                plate = "NOPLATE"
            logging.info("Clase: " +str(Result)+" Ejes: "+ str(NumWheels) +" Placa: "+str(plate))
            
            if int(Result)!=0:
                # se genera las direcciones ip y el puerto configurados
                dic = self.DevicesModel.globalDic["Com"]
                ip = str(dic["ip_0"])+"."+str(dic["ip_1"])+"."+str(dic["ip_2"])+"."+str(dic["ip_3"])
                port = str(dic["tcp_port"])
                # se adiciona los nuevos resultados al buffer
                self.comBuffer.append([Result,Right,NumWheels,plate])
                # se crea la conexion
                com = DataSending(ip,port)
                # se verifica si existe una conexion con el servidor
                if com.getData():
                    # para cada una de los registros en el buffer se intenta realizar el envio 
                    for i in range(len(self.comBuffer)):
                        Result,Right,NumWheels,plate = self.comBuffer[i]
                        # si el envio es  exitoso se elimina el registro del buffer
                        if com.sendData(int(Result),Right,int(NumWheels),plate):
                            del self.comBuffer[i]
                # finalmente se cierra la conexion con el servidor
                com.comclose()
        else:
            pass
 
    def startThreads(self,laser_list,radar_list):
    	# Se Inician todos los hilos
    	#NOTA: en este metodo se limpian los buffers, si no se desea eso no utilizar este metodo, o modificarlo
        # time.sleep(5.0)
        
        self.error_count = 0
        self.threads_J = []
        self.hilo_1_Recv = Queue()

        self.hilo_2_Recv=Queue()

        self.hilo_3_Recv=Queue() # recibe los datos del hilo 3

        self.hilo_4_Recv=Queue() # recibe los datos del hilo 3

        self.hilo_5_Recv=Queue() # recibe los flags del hilo  4 y los envia al hilo 5

        #Indica si los hilos iniciaron correctamente
        bool_out = False

        # try:

        self.radar_ports = self.DevicesModel.Radar_Dic.keys()
        # print self.radar_ports
        self.laser_list = laser_list
        laser_type, laser_port =laser_list[0]
        if laser_type=="Hokuyo-utm30-lx":
            self.prototype_b = False
            os.chmod(laser_port, 484)
            for ports in radar_list :
                if self.DevicesModel.Radar_Dic[ports[1]].Plugged:
                    os.chmod(str(ports[1]), 484)

            self.laser_p = multiprocessing.Process(name='laser', target=laserobt, args=(self.DevicesModel.Hokuyo_Dic[laser_port], self.DevicesModel.Radar_Dic[str(self.radar_ports [0])], 
                self.DevicesModel.Radar_Dic[str(self.radar_ports [1])], self.DevicesModel.Sample_C_Iz, self.DevicesModel.Sample_C_De, self.DevicesModel.Hokuyo_Dic[laser_port].angle_a0, 
                self.hilo_1_Recv,self.hilo_5_Recv))

        else:
            self.prototype_b = True
            self.laser_p = multiprocessing.Process(name='laser', target=rplidarobt, args=(self.DevicesModel.Laser_Dic[laser_port],
            self.DevicesModel.Sample_C_Iz, self.DevicesModel.Sample_C_De, self.DevicesModel.Laser_Dic[laser_port].angle_a0, 
                self.hilo_1_Recv,))

        self.Inicial_Cloud = multiprocessing.Process(name='inicial_cloud', target=CloudCreacionobt, args=(self.hilo_1_Recv, self.hilo_2_Recv, self.DevicesModel.Creation_C_IZQ,
            self.DevicesModel.Creation_C_DER, self.DevicesModel.Creation_RPC_IZQ,self.DevicesModel.Creation_RPC_DER, self.prototype_b,))
        self.Cluster_Descriptor = multiprocessing.Process(name='cluster_descriptor', target=ClusterAndDecriptorCpp, args=(self.hilo_2_Recv, self.hilo_3_Recv, 33, 
            self.DevicesModel.Segmentador_C, self.DevicesModel.Descriptor_C, self.DevicesModel.PassCloudClasses,))
        self.clasiffier = multiprocessing.Process(name='clasiffier', target=Clasificate, args=(self.hilo_3_Recv, self.hilo_4_Recv, self.DevicesModel.SVM_C,
            self.prototype_b,))
        #self.licenseplate = multiprocessing.Process(name='licenseplate', target=PlateDetection, args=(self.DevicesModel.Cameras,self.hilo_5_Recv,))
        
        self.threads_J.append(self.laser_p)
        self.threads_J.append(self.Inicial_Cloud)
        self.threads_J.append(self.Cluster_Descriptor)
        self.threads_J.append(self.clasiffier)
        
        for j in self.threads_J:
            j.start()

            # for j in self.threads_J:
            #     j.join()

            #Se inicia el timer
        self.timer.start()

        bool_out = True
        # return bool_out

        # except Exception as e:
        # 	logging.warning("Error: unable to start thread")
        # else:
        # 	pass
        # finally:
        # 	return bool_out

		# return bool_out




    def stopThreads(self):
        #se detiene el timer y se detienen los hilos
        print "STOOP"
        self.timer.stop()
        if self.laser_list is not None:
            laser_type, laser_port =self.laser_list[0]
            if laser_type=="Hokuyo-utm30-lx":
				Laser = self.DevicesModel.Hokuyo_Dic[laser_port]
				radar_1 = self.DevicesModel.Radar_Dic[str(self.radar_ports [0])]
				radar_2 = self.DevicesModel.Radar_Dic[str(self.radar_ports [1])]
				if Laser.laser.is_open():
				    Laser.LaserDispose()
				else:
					pass

				if radar_1.Plugged:
				    radar_1.disconnect()
				else:
					pass

				if radar_2.Plugged:
				    radar_2.disconnect()
				else:
					pass

        for j in self.threads_J:
            j.terminate()
            j.join()

    

    def setDeviceModel(self,DevicesModel):
        self.DevicesModel = DevicesModel

    def setSoftwareModels(self,SoftwareModels):
        self.DevicesModel = SoftwareModels


def laserobt(Laser, radar_1, radar_2, selection_Iz, selection_De, angle_samples, q_send, lp_send =None):
    Ini=time.time()
    lpflag_der =selection_De.esperar
    lpflag_izq =selection_Iz.esperar
    
    velocity = 1.0
    velocity_1 = -1.0
    velocity_2 = -1.0

    if radar_1.Plugged:
        radar_1.connect()
        radar_1.configSensor()
        # radar_1.SetSensibility(radar_1.__dict__["sensibility"])
        radar_1.clearBuffer()
        velocity_1 = velocity

    if radar_2.Plugged:
        radar_2.connect()
        radar_2.configSensor()
        # radar_2.SetSensibility(radar_2.__dict__["sensibility"])
        radar_2.clearBuffer()
        velocity_2 = velocity


    if not Laser.laser.is_open():
    	Laser.OpenDevice()
    	# time.sleep(1.0)

    try:
        while (True):
            if velocity_1>=0:
                if(radar_1.samplesinbuffer()):
                    aux = radar_1.ReadData()
                    if len(aux)>0:
                        velocity_1 = aux[-1]

            if velocity_2>=0:
                if(radar_2.samplesinbuffer()):
                    aux = radar_2.ReadData()
                    if len(aux)>0:
                        velocity_2 = aux[-1]

            x = Laser.getRangeScanAndAppend(velocity,time.time()-Ini)
            aux = x[:(1080/2)+2+1]
            aux[0] = velocity_2
            aux_2 = np.concatenate((x[:2],np.asarray(x[(1080/2)+2:])),axis=0)
            aux_2[0] = velocity_1
            
            # if velocity_2>=0:
            aux[0]=velocity_1
            if velocity_1>=0 and Laser.d_enable:
                
                # print "Der", angle_samples-45, selection_De.start, selection_De.stop
                selection_De.selection(aux_2, angle_samples-45, selection_De.start, selection_De.stop, selection_De.step, selection_De.min_dis, selection_De.max_dis, selection_De.minNoVehicle, selection_De.minSamples)
                if selection_De.esperar!=lpflag_der:
                    lpflag_der = selection_De.esperar
                    now = datetime.datetime.now()
                        
                    if (not selection_De.esperar) and lp_send!=None:
                        Tiempo_D=str(now.year)+"_"+str(now.month)+"_"+str(now.day)+"_"+str(now.hour)+"_"+str(now.minute)+"_"+str(now.second)+"_D"
                        lp_send.put([Tiempo_D,"Derecho"])
            
            if velocity_1>=0 and Laser.i_enable:
                # print "dos"
                selection_Iz.selection(aux, angle_samples, selection_Iz.start, selection_Iz.stop, selection_Iz.step, selection_Iz.min_dis, selection_Iz.max_dis, selection_Iz.minNoVehicle, selection_Iz.minSamples)
                if selection_Iz.esperar!=lpflag_izq:
                    lpflag_izq = selection_Iz.esperar
                    now = datetime.datetime.now()
                    if (not selection_Iz.esperar) and lp_send!=None:
                        Tiempo_I=str(now.year)+"_"+str(now.month)+"_"+str(now.day)+"_"+str(now.hour)+"_"+str(now.minute)+"_"+str(now.second)+"_I"
                        lp_send.put([Tiempo_I,"Izquierdo"])


            if (selection_De.finalizo):
                now = datetime.datetime.now()
                #Tiempo=str(now.hour)+"_"+str(now.minute)+"_"+str(now.second)+"_Derecha"
                selection_De.finalizo = False
                logging.info('ACAbe bloquer, %s para dere ', Tiempo_D)
                Right_C = True

                q_send.put([Tiempo_D,selection_De.data_out, Right_C])

            if (selection_Iz.finalizo):
                now = datetime.datetime.now()
                #Tiempo=str(now.hour)+"_"+str(now.minute)+"_"+str(now.second)+"_Izquierda"
                selection_Iz.finalizo = False
                logging.info('ACAbe bloquer, %s para LEFT ', Tiempo_I)
                Right_C = False

                q_send.put([Tiempo_I,selection_Iz.data_out, Right_C])

    except KeyboardInterrupt:
        logging.info("stopping")
    else:
    	pass 
    finally:
        radar_1.disconnect()
        radar_2.disconnect()
        Laser.LaserDispose()
        logging.info('Stoping Hilo1')

def rplidarobt(Laser, selection_Iz, selection_De, angle_samples, q_send):
    lpflag_der =selection_De.esperar
    lpflag_izq =selection_Iz.esperar


    if not Laser.is_open():
        os.chmod(dev_port,484)             
        Laser.connect()
    Laser.stop_motor()
    time.sleep(1)
    Laser.start_motor()
    time.sleep(1)
    Laser.init_read_scans()
    time.sleep(1)
    
    try:
        time_offset = time.time()
        time_iz = []
        time_de = []
        time_iz_offset = 0
        time_de_offset = 0
        while (True):
            data = Laser.read_scans()
            
            if data != []:
                iz_idx =  np.logical_and(data[:,1]>=Laser.i_range_min,data[:,1]<=Laser.i_range_max)
                de_idx =  np.logical_and(data[:,1]>=Laser.d_range_min,data[:,1]<=Laser.d_range_max)

                data_iz  = data[iz_idx,:]
                data_de  = data[de_idx,:]
                selection_Iz.selectionRplidar(data_iz, 90.)
                selection_De.selectionRplidar(data_de, 270.,inverted = True)

                if selection_Iz.esperar!=lpflag_izq:
                    if not selection_Iz.esperar:
                        time_iz=[0]
                        time_iz_offset =time.time()
                
                time_iz.append(time.time()-time_iz_offset)
                lpflag_izq = selection_Iz.esperar

                if selection_De.esperar!=lpflag_der:
                    if not selection_De.esperar:
                        time_de=[]
                        time_de_offset =time.time()
                time_de.append(time.time()-time_de_offset)
                lpflag_der = selection_De.esperar
                

                if (selection_De.finalizo):
                    now = datetime.datetime.now()
                    Tiempo=str(now.hour)+"_"+str(now.minute)+"_"+str(now.second)+"_Derecha"
                    selection_De.finalizo = False
                    logging.info('ACAbe bloquer, %s para dere ', Tiempo)
                    Right_C = True
                    q_send.put([Tiempo,np.array(selection_De.data_out), Right_C])

                if (selection_Iz.finalizo):
                    now = datetime.datetime.now()
                    Tiempo=str(now.hour)+"_"+str(now.minute)+"_"+str(now.second)+"_Izquierda"
                    selection_Iz.finalizo = False
                    logging.info('ACAbe bloquer, %s para LEFT ', Tiempo)
                    Right_C = False
                    q_send.put([Tiempo,np.array(selection_Iz.data_out), Right_C])

    except KeyboardInterrupt:
        logging.info("stopping")
    else:
    	pass 
    finally:
        logging.info('Stoping Hilo1')


def CloudCreacionobt(recv,send, new_p_I_H, new_p_D_H, new_p_I_RP, new_p_D_RP, prototype):
    if prototype:
        new_p_I = new_p_I_RP
        new_p_D = new_p_D_RP
    else:
        new_p_I = new_p_I_H
        new_p_D = new_p_D_H  
    try:
        while True:
            # print "limits", new_p_D.x_limit, new_p_D.y_limit
            if (not recv.empty()):

                # print  new_p_D.Left, "MIRARA", new_p_D.angles[0], new_p_D.angles[-1], new_p_D.InicialOffset, new_p_D.FinalOffset, new_p_D.total_range_angle
                ## se recibe la etiqueta de tiempo, el bloque de laser del hokuyo y el bloque de laser del Rplidar con su vector de velocidades
                TiempoV, data_laser, Right= recv.get()
                # print "EHHHH"
                # np.savetxt('./guardar/_data'+TiempoV+'.out',data_laser)
                
                # np.savetxt('./Preprocesados/_data'+TiempoV+'.out',data_laser)
                ## Se procesa la nube de puntos para el hokuyo
                if (Right):
                    new_p_D.Put_Laser(data_laser)
                    new_p_D.Point_cloud_Create(TiempoV, new_p_D.save_original)
                    print "limits", new_p_D.x_limit, new_p_D.y_limit
                    new_p_D.Filter_distance(new_p_D.original_pointcloud, new_p_D.x_limit, new_p_D.y_limit, TiempoV, new_p_D.save_filtered)
                    # print "hola", new_p_D.x_limit, new_p_D.y_limit
                    if (not new_p_D.error):
                        new_p_D.Ground_extraction_Normal(new_p_D.filtered_pointcloud_d, TiempoV,new_p_D.save_No_ground)
                        angleX = new_p_D.CalculateAngle(new_p_D.TanAngleX)
                        angleZ = new_p_D.CalculateAngle(new_p_D.TanAngleZ)
                        # borrar()
                        # print "NO HAY ERROR"

                    if (not new_p_D.error):
                        nube_pasar = np.asarray(new_p_D.without_plane_pointloud, dtype=np.float64)
                        # print "ENTRANDO"
                        send.put([nube_pasar, TiempoV, angleX, angleZ, Right])
                else:
                    new_p_I.Put_Laser(data_laser)
                    new_p_I.Point_cloud_Create(TiempoV, new_p_I.save_original)
                    new_p_I.Filter_distance(new_p_I.original_pointcloud, new_p_I.x_limit, new_p_I.y_limit, TiempoV,  new_p_I.save_filtered)
                    # print "hola", new_p_I.x_limit, new_p_I.y_limit
                    if (not new_p_I.error):
                        new_p_I.Ground_extraction_Normal(new_p_I.filtered_pointcloud_d, TiempoV,new_p_I.save_No_ground)
                        angleX = new_p_I.CalculateAngle(new_p_I.TanAngleX)
                        angleZ = new_p_I.CalculateAngle(new_p_I.TanAngleZ)
                        # borrar()
                        # print "NO HAY ERROR"

                    if (not new_p_I.error):
                        nube_pasar = np.asarray(new_p_I.without_plane_pointloud, dtype=np.float64)
                        # print "ENTRANDO"
                        send.put([nube_pasar, TiempoV, angleX, angleZ, Right])
            else:
                time.sleep(0.02)
                

    except KeyboardInterrupt:
        pass
    # else:
    # 	pass    
    finally:
        logging.info('Stoping Hilo2.')

def ClusterAndDecriptorCpp(recv,send, PointDescriptor, segmentador, Descriptor, PassCloudClasses):
    # segmentador = Central_C.Segmentador_C
    # Descriptor = Central_C.Descriptor_C

    ### Variables para el diccionario en C++ ###

    cloud_filtered = "cloud_filtered"
    cloud_cluster = "cloud_cluster"
    cloud_clustered_demean = "cloud_clustered_demean"
    cloud_clustered_filtered = "cloud_clustered_filtered"
    InitView = True
    Inicial_distance = segmentador.Wheels_distance_filter
    #############################################

    # try:
    while True:
        if (not recv.empty()):
            nube_pasar, TiempoV1, angleX, angleZ, Right= recv.get()
            segmentador.passCloud(cloud_filtered,nube_pasar)
            segmentador.GetIndicesGlobal_B(cloud_filtered)

            size = segmentador.getSizeClusterGlobal()
            it3 = 0

            for it3 in xrange(0,size):
                if (size>1):
                    segmentador.GetClusterGlobal(cloud_filtered,cloud_cluster,it3)
                    segmentador.DemeanCluster(cloud_cluster,cloud_clustered_demean)
                else:
                    segmentador.GetClusterGlobal(cloud_filtered,cloud_cluster,it3)
                    segmentador.DemeanCluster(cloud_cluster,cloud_clustered_demean)

                segmentador.filter_outliers_B(cloud_clustered_demean,cloud_clustered_filtered)
                segmentador.Angle_Rotate(cloud_clustered_filtered, cloud_clustered_filtered, angleX, angleZ)

                vectorMin = np.zeros((3,), dtype=np.float64)
                vectorMax = np.zeros((3,), dtype=np.float64)
                vectorCent = np.zeros((3,), dtype=np.float64)
                segmentador.CalculateMinMax(cloud_filtered, vectorMin, vectorMax, vectorCent)


                FirstEje_Vector = np.zeros((2,), dtype=np.float64)
                LastEje_Vector = np.zeros((2,), dtype=np.float64)
                entero = segmentador.CountWheels_B(cloud_clustered_filtered, FirstEje_Vector, LastEje_Vector)
                segmentador.Wheels_distance_filter = Inicial_distance

                if (entero>=2):
                    referenciaa = FirstEje_Vector[0]                
                    altura = segmentador.CalculateHeightFirstEje(cloud_clustered_filtered,referenciaa, 0.2)
                    largoXY = FirstEje_Vector[1] - LastEje_Vector[0] 

                else:
                    FirstEje_Vector = np.zeros((2,), dtype=np.float64)
                    LastEje_Vector = np.zeros((2,), dtype=np.float64)
                    segmentador.Wheels_distance_filter = 0.07
                    entero_Aux = segmentador.CountWheels_B(cloud_clustered_filtered, FirstEje_Vector, LastEje_Vector)
                    if (entero_Aux==2):
                        referenciaa = FirstEje_Vector[0]                
                        altura = segmentador.CalculateHeightFirstEje(cloud_clustered_filtered,referenciaa, 0.2)
                        largoXY = FirstEje_Vector[1] - LastEje_Vector[0]
                        entero = entero_Aux
                    else:
                        altura= segmentador.CalculateHeight(cloud_clustered_filtered)
                        largoXY = vectorMax[2] - vectorMin[2]

                vector_geometria = []
                vector_geometria.append([largoXY, altura, entero, vectorMax[1]-vectorCent[1], vectorCent[2]-vectorMin[2]])

                PassCloudClasses(segmentador,Descriptor, cloud_clustered_filtered)
                Descriptor.CalculateNormals_B()
                Descriptor.UniformSampling_B();
                Descriptor.CalculateFPFHSampling_B();

                if (segmentador.Cluster_save):
                    segmentador.saveCluster(cloud_clustered_filtered,str(segmentador.Cluster_Folder)+"/"+TiempoV1+"_cluster_filtered",it3)
                else:
                    pass

                DescriptorsNumber=Descriptor.putWidth()
                DescriptorVector = np.zeros(shape=(DescriptorsNumber,33))
                for i in range(0,DescriptorsNumber):
                    for j in range(0,33):
                        DescriptorVector[i,j] = Descriptor.getValueFPFH(i,j)
                
                # logging.info("Largo: " , largo, " altura: ", altura, " ruedas:", entero)
                # print "revision1", len(vector_geometria)
                send.put([vector_geometria,DescriptorVector, TiempoV1, Right])

        else:
            time.sleep(0.02)

    # except KeyboardInterrupt:
    #     pass
    # else:
    # 	pass 
    # finally:
    #     logging.info('Stoping Hilo3')

def Clasificate(recv,send, model, prototype):
    
    try:
        while True:
            # print "REVISAR"
            if (not recv.empty()):
                vector_geometria, DescriptorVector, label, Right = recv.get()
                
                vector_geometria_aux = []
                if prototype:
                    model.prototype = True
                    vector_geometria_aux.append(vector_geometria[-1][1])
                else:
                    vector_geometria_aux.append(vector_geometria[-1][:])
                result = model.predict(DescriptorVector, model.km_selected , model.svm_selected,vector_geometria_aux)
                # logging.info('%s es: %s', label, str(Vector_categorias[int(result)]))
                result_int = int(result)
                if result_int == 4:
                    if vector_geometria[-1][2] == 2:
                        result_int = 4
                    if vector_geometria[-1][2] == 3:
                        result_int = 5
                    if vector_geometria[-1][2] == 4:
                        result_int = 6
                    if vector_geometria[-1][2] == 5:
                        result_int = 7
                    if vector_geometria[-1][2] == 6:
                        result_int = 8
                elif result_int == 5:
                    result_int = 4
                else:
                    pass
                send.put([result_int, vector_geometria[-1][2],label, Right])

            else:
                time.sleep(0.02)

    except KeyboardInterrupt:
        pass
    else:
    	pass 
    finally:
        logging.info('Stoping Hilo4')

def PlateDetection(Cams,recv,LPR,platebuffer,save = True):
    CamsIds =Cams.GetCamerasId()
    while (not recv.empty()):
        TiempoV,line  = recv.get()
        for camId in CamsIds:
            if Cams.GetCamera(camId).Carril ==line:
                success, img = Cams.getFrame(camId)
                if success:
                    platebuffer[TiempoV] = LPR.RecognizePlatesWithCNN(img,size=(96,48),scale=1.1,neighbors=2,show=False,scorethd =0.3,biggestPlate=True).keys()[0]
                    if save:
                        cv2.imwrite('im_' +str(TiempoV)+'.png' , img)
    return platebuffer

    # except KeyboardInterrupt:
    #     pass
    # else:
    # 	pass 
    # finally:
    #     pass



if __name__ == '__main__':
    
 
    os.system('cls' if os.name == 'nt' else 'clear')
    sys.path.insert(0, '/home/jhon-kevin/Documentos/all_project/Repositorio/Interface/Backend/')
    from DeviceModel import DevicesModel
    from SoftwareModel_Class import Parameters_class
    # logging.getLogger().setLevel(logging.DEBUG)
    modelH = DevicesModel("/home/jhon-kevin/Documentos/all_project/Repositorio/Interface/Backend/parametros")
    modelSoft = Parameters_class("/home/jhon-kevin/Documentos/all_project/Repositorio/Interface/Backend/default_parameters")
    # ex = LaserHarwtab()
    Continue =  False
    Controller = ThreadsController(modelH, modelSoft)
    # ex.show()
    # while not Continue:
    Continue = Controller.startThreads()
    try:
        while 1:
            pass

    except KeyboardInterrupt:
        logging.info("acabando TODO")
    # sys.exit(app.exec_())
    else:
    	pass 
    finally:
        Controller.stopThreads()