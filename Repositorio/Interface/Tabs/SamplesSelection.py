#!/usr/bin/env python
# -*- coding: utf-8 -*-

import sys
from  PyQt4 import QtGui,QtCore
from Utils import polar2rec
import os
import numpy as np
HOKUYO_BASEDIR = os.path.dirname(os.path.abspath(__file__))
sys.path.insert(0, HOKUYO_BASEDIR+u'/../VisualizationWidgets')
from vispy.app import Timer
from VisualizationWidgets.VisualizationWidgets import PlotDataWidget,MyButton,TextAndSpinBox,ResetButton,MyQComboBox,MyQSpinBox,HelpWindow,ConfigurationWidget
from Constants import *
#from VisualizationWidgets import *




class SamplesSelectionController:
 
    DevicesModel = None
    SamplesSelectionTab = None
    lastdevice = None
    timer = None
    UpdatingParameters =False
    Device = None
    isHokuyo =False
    def __init__(self, SamplesSelectionTab, DevicesModel, prototype=False, timerinterval ='auto'):
        # se guarda el modelo y la vista en los atributos de las clases
        self.SamplesSelectionTab = SamplesSelectionTab
        self.prototype = prototype
        self.DevicesModel = DevicesModel
        # Se definen los rangos de los spinbox
        self.SetSpinboxsRanges()
        # se actualizan los puertos existentes
        self.update_line_list()
        self.update_line()
        # Evento cuando se elije un dispositivo diferente
        self.SamplesSelectionTab.menuWidget.Carril.currentIndexChanged.connect(self.update_line)
        # evento cuando las configuraciones cambian
        self.SamplesSelectionTab.menuWidget.signal_objet_changed.connect(self.update_param)
        # se crea la ventana de ayuda y se carga la imagen y se crea la interrupcion
        self.SamplesSelectionTab.menuWidget.helpButton.clicked.connect(self.help)
        self.helpWindow = HelpWindow(HOKUYO_BASEDIR+u'/../VisualizationWidgets/help_ss1.png',parent=self)
        #se  crea un reloj que permitira actualizar la adquisicion de datos de los sensores emulando una interupcion y se guarda el intervalo predefinido
        

        self.timerinterval = timerinterval
        self.timer = Timer(interval= timerinterval,connect= self.timerevent)
        self.SamplesSelectionTab.menuWidget.resetButton.clicked.connect(lambda: self.update_menu(RESET))
        self.polar2rec =polar2rec(-45)
        
    def update_menu(self,mode):
        self.UpdatingParameters =True
        line_key = self.getcurrentLine()
        if mode == UPDATE:
            Source_Dic = self.DevicesModel.globalDic["Sample_Selection"][line_key]
        else:
            Source_Dic = self.DevicesModel.DefaultDic["Sample_Selection"][line_key]

        menu_dic = self.SamplesSelectionTab.menuWidget.__dict__
        for key in Source_Dic:
            menu_dic[key].setValue(Source_Dic[key])
        self.UpdatingParameters =False
        if mode == RESET:
            self.update_param()
        self.CreatePlot()

    def CreatePlot(self):    
        if self.Device !=None:
            
            line_key = self.getcurrentLine()
            Source_Dic = self.DevicesModel.globalDic["Sample_Selection"][line_key]
            device = self.Device 
            ones =np.ones((1081),dtype=np.float)*1000
            if self.prototype:
                if line_key == "Derecho":              
                    beam0 = self.angle2Beam(0)
                    start = beam0 + Source_Dic["Laser_start"]
                    Stop = beam0  - Source_Dic["Laser_stop"]
                else:
                    beam0 = self.angle2Beam(180)
                    Stop = beam0 - Source_Dic["Laser_start"]
                    start = beam0  + Source_Dic["Laser_stop"]

            else:
                if line_key == "Izquierdo":             
                    beam0 = self.angle2Beam(0)
                    start = beam0 + Source_Dic["Laser_start"]
                    Stop = beam0  - Source_Dic["Laser_stop"]
                else:
                    beam0 = self.angle2Beam(180)
                    Stop = beam0 - Source_Dic["Laser_start"]
                    start = beam0  + Source_Dic["Laser_stop"]

            width =  start - Stop
            
            X_min,Y_min = self.polar2rec.pol2rec(ones*Source_Dic["Distance_min"],-45)
            X_max,Y_max = self.polar2rec.pol2rec(ones*Source_Dic["Distance_max"],-45)
            X  = np.empty((width*2+1,), dtype=np.float32)
            X[0:width] = X_min[Stop:start]
            X[width:2*width] = X_max[start:Stop:-1]
            X[-1] = X_min[Stop]

            Y  = np.empty((width*2+1,), dtype=np.float32)
            Y[0:width] = Y_min[Stop:start]
            Y[width:2*width] = Y_max[start:Stop:-1]
            Y[-1] = Y_min[Stop]
            self.SamplesSelectionTab.plotCanvas.CreatePolygon(ROI_KEY,X,Y,LIMIT_COLOR)

                    



    def angle2Beam(self,angle):
        return 4*angle+180

    def update_line(self):
        print self.getcurrentLine()
        
        self.update_menu(UPDATE)

    def update_line_list(self):
        if self.DevicesModel.globalDic != None:
            line_list =self.DevicesModel.globalDic["Sample_Selection"].keys()
            self.SamplesSelectionTab.menuWidget.Carril.addItems(line_list)          
            self.update_menu(UPDATE)


    def update_param(self):
        if not self.UpdatingParameters: 
            line_key = self.getcurrentLine()
            Source_Dic = self.DevicesModel.globalDic["Sample_Selection"][line_key]
            
            menu_dic = self.SamplesSelectionTab.menuWidget.__dict__
            for key in Source_Dic:
                Source_Dic[key] = menu_dic[key].value()
            self.CreatePlot()
    
    def timerevent(self,event):
        # if self.Device !=None:
        #     if self.Device.Plugged:
        #         if self.Device.laser.is_open():
        #             success =True
        #             try:
        #                 data =self.Device.getRangeScanAndAppend(0,0)
        #             except:
        #                 success = False 

        #             if success:
        #                 device = self.Device
        #                 X,Y = self.polar2rec.pol2rec(data,device.angle_a0)
        #                 self.SamplesSelectionTab.plotCanvas.CreateLine(IZQ_KEY,X[device.i_range_min:device.i_range_max],Y[device.i_range_min:device.i_range_max],IZQ_COLOR)
        #                 self.SamplesSelectionTab.plotCanvas.CreateLine(DER_KEY,X[device.d_range_min:device.d_range_max],Y[device.d_range_min:device.d_range_max],DER_COLOR)
                
        # else:
        #     self.update_device()


        if self.Device !=None:
            if self.Device in self.DevicesModel.Hokuyo_Dic.values():
                success = True
                if self.Device.is_open():
                    try:
                        data = self.Device.getRangeScanAndAppend(0,0)
                    except:
                        success = False 
                    if success:
                        X,Y = self.polar2rec.pol2rec(data,self.Device.angle_a0)
                        if (self.Device.i_enable):
                            self.SamplesSelectionTab.plotCanvas.CreateMarkers(IZQ_KEY,X[self.Device.i_range_min:self.Device.i_range_max],Y[self.Device.i_range_min:self.Device.i_range_max],IZQ_COLOR)
                        if (self.Device.d_enable):
                            self.SamplesSelectionTab.plotCanvas.CreateMarkers(DER_KEY,X[self.Device.d_range_min:self.Device.d_range_max],Y[self.Device.d_range_min:self.Device.d_range_max],DER_COLOR)
                else:
                    self.update_device()
            if self.Device in self.DevicesModel.Laser_Dic.values():
                success = True
                if self.Device.is_open():
                    try:
                        data = np.array(self.Device.read_scans())
                    except:
                        success = False 
                    if success:
                        if data != []:
                            X,Y = polar2rec.Pol2Rec(data[:,1],data[:,2],xaxisAngle=self.Device.angle_a0)
                            
                            iz_idx =  np.logical_and(data[:,1]>=self.Device.i_range_min,data[:,1]<=self.Device.i_range_max)
                            de_idx =  np.logical_and(data[:,1]>=self.Device.d_range_min,data[:,1]<=self.Device.d_range_max)

                            if (self.Device.i_enable):
                                self.SamplesSelectionTab.plotCanvas.CreateMarkers(IZQ_KEY,X[iz_idx],Y[iz_idx],IZQ_COLOR)
                            if (self.Device.d_enable):
                                self.SamplesSelectionTab.plotCanvas.CreateMarkers(DER_KEY,X[de_idx],Y[de_idx],DER_COLOR)
                
                else:
                    self.update_device()

    def SetSpinboxsRanges(self):
        menu_dic = self.SamplesSelectionTab.menuWidget.__dict__
        for key in SAMPLES_SELECTION_SPINBOX:
            params = SAMPLES_SELECTION_SPINBOX[key]
            
            menu_dic[key].setMinimum(params[0])
            menu_dic[key].setMaximum(params[1])
            if len(params)==3:
                menu_dic[key].setSingleStep(params[2])

    def getcurrentLine(self):
        port = str(self.SamplesSelectionTab.menuWidget.Carril.currentText())
        return port

    def startAdquisition(self):
        self.timer.start()
        self.update_device()

    def stopAdquisition(self):
        self.timer.stop()
        try:
            if self.Device != None:
                self.Device.LaserDispose()    
        except:
            pass
    
    def update_device(self):
        dev_port =''
        
        for key, value in self.DevicesModel.Laser_Dic.iteritems():
            if value.Plugged:
                dev_port = key
                self.Device =value
                self.isHokuyo =False
        for key, value in self.DevicesModel.Hokuyo_Dic.iteritems():
            if value.Plugged:
                dev_port = key
                self.Device =value
        if dev_port != '':
            self.DevicesModel.OpenLaser(dev_port)
            self.CreatePlot()
    
    def help(self):
        self.helpWindow.show() 
    


class SampleSelectionMenuWidget(ConfigurationWidget):
    
    def __init__(self,maxWidth=300):
        super(SampleSelectionMenuWidget, self).__init__(label=u"Panel de Control")
        
        
        l_Carril = QtGui.QGroupBox(u"Seleccionar Carril:")
        l_Carril_l = QtGui.QHBoxLayout()
        self.Carril = MyQComboBox()
        l_Carril_l.addWidget(self.Carril)
        l_Carril.setLayout(l_Carril_l)
        self.gbox.addRow(l_Carril)
        
        # group Box de Carriles
        l_Valid_Samples = QtGui.QGroupBox("Muestras Validas:")
        l_Valid_Samples_l = QtGui.QFormLayout()

        self.Min_Vehicles_Samples = MyQSpinBox()
        self.Min_Vehicles_Samples.valueChanged.connect(self.update_param)
        l_Valid_Samples_l.addRow(TextAndSpinBox(u"Muestras Validas:",self.Min_Vehicles_Samples,0,0))
        self.Min_No_Vehicles = MyQSpinBox()
        self.Min_No_Vehicles.valueChanged.connect(self.update_param)
        l_Valid_Samples_l.addRow(TextAndSpinBox(u"Corte:",self.Min_No_Vehicles,0,0))
        self.Vector_Temporal_Size = MyQSpinBox()
        self.Vector_Temporal_Size.valueChanged.connect(self.update_param)
        l_Valid_Samples_l.addRow(TextAndSpinBox(u"Muestras en Buffer:",self.Vector_Temporal_Size,0,0))
        l_Valid_Samples.setLayout(l_Valid_Samples_l)
        self.gbox.addRow(l_Valid_Samples)

        l_Distancia = QtGui.QGroupBox("Distancia:")
        l_Distancia_l = QtGui.QFormLayout()
        self.Distance_min = QtGui.QDoubleSpinBox()
        self.Distance_min.valueChanged.connect(self.update_param)
        l_Distancia_l.addRow(TextAndSpinBox(u"Min:",self.Distance_min,0,0))
        self.Distance_max = QtGui.QDoubleSpinBox()
        self.Distance_max.valueChanged.connect(self.update_param)
        l_Distancia_l.addRow(TextAndSpinBox(u"Max:",self.Distance_max,0,0))
        l_Distancia.setLayout(l_Distancia_l)
        self.gbox.addRow(l_Distancia)

        l_Beams = QtGui.QGroupBox("Haces de luz:")
        l_Beams_l = QtGui.QFormLayout()
        self.Laser_start = MyQSpinBox()
        self.Laser_start.valueChanged.connect(self.update_param)
        l_Beams_l.addRow(TextAndSpinBox(u"Superior:",self.Laser_start,0,0))
        self.Laser_stop = MyQSpinBox()
        self.Laser_stop.valueChanged.connect(self.update_param)
        l_Beams_l.addRow(TextAndSpinBox(u"Inferior:",self.Laser_stop,0,0))
        self.Laser_step = MyQSpinBox()
        self.Laser_step.valueChanged.connect(self.update_param)
        l_Beams_l.addRow(TextAndSpinBox(u"Salto:",self.Laser_step,0,0))
        l_Beams.setLayout(l_Beams_l)
        self.gbox.addRow(l_Beams)
    







class SamplesSelectionTab(QtGui.QWidget):
    
    def __init__(self, parent=None,timerinterval=0.025):
        
        super(SamplesSelectionTab, self).__init__(parent)
        # se crea el layout a utilizar y se disponen los elementos

        F_layout = QtGui.QHBoxLayout() 
        self.plotCanvas = PlotDataWidget()
        self.plotCanvas.create_native()

        self.menuWidget = SampleSelectionMenuWidget()
        self.plotCanvas.native.setParent(self)
        F_layout.addWidget(self.menuWidget)
        F_layout.addWidget(self.plotCanvas.native)
        self.Next = MyButton("Siguiente")
        S_layout = QtGui.QHBoxLayout() 
        S_layout.addStretch(1)
        S_layout.addWidget(self.Next)
        V_layout = QtGui.QVBoxLayout()
        V_layout.addLayout(F_layout)
        V_layout.addLayout(S_layout)
        self.setLayout(V_layout)


        #self.Next.clicked.connect(self.next_tab)
        # se crean los eventos a utlizar  y se inicializa el timer para capturar los datos de los sensores

    def setSesorsList(self, HokuyoList):
        self.menuWidget.puertos.addItems(HokuyoList)

if __name__ == '__main__':
    
 
    os.system('cls' if os.name == 'nt' else 'clear')
    sys.path.insert(0, './Backend')
    from Backend.DeviceModel import DevicesModel
    import logging
    logging.getLogger().setLevel(logging.DEBUG)
    app = QtGui.QApplication(sys.argv)
    model = DevicesModel("/home/ssi_ralvarez/Documentos/all_project/Repositorio/Interface/Backend/parametros")
    ex = SamplesSelectionTab()
    ex.show()
    sys.exit(app.exec_())

