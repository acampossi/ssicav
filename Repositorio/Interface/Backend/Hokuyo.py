#!/usr/bin/env python
import os
import hokuyoaist
import numpy as np
import time
import copy

## Clase para la lectura y configuracion del sensor Hokuyo utm 30lx.
# @param portParameters Parametros de configuracion del sensor en la que se define el tipo de conexion, el puerto y el timeout
# @param Plugged  Booleano que indica si el Sensor esta conectado 
# @param i_enable Booleano que indica su el  Carril Izquierdo esta habilitado
# @param i_range_min  Rango minimo del carril Izquierdo
# @param i_range_max  Rango minimo del carril Izquierdo
# @param d_enable  Booleano que indica su el Carril Derecho Habilitado
# @param d_range_min Rango minimo Carril Derecho
# @param d_range_max Rango maximo Carril Derecho
# @param angle_a0 Angulo  que se forma entre el haz 0 del sensor y el eje x 
# @param step  Resolucion angular del sensor Hokuyo
# @param laser Instanacia de la clase sensor de la libreria hokuyoaist 


class Hokuyo_utm():
    portParameters= None  #Hokuyo port name
    Plugged = False       # El Sensor esta conectado 
    i_enable = True       # El Carril Izquierdo esta habilitado
    i_range_min = 0       # Rango minimo del carril Izquierdo
    i_range_max = 540     # Rango minimo del carril Izquierdo
    d_enable = True       # Carril Derecho Habilitado
    d_range_min = 541     # Rango minimo Carril Derecho
    d_range_max = 1080    # Rango maximo Carril Derecho
    angle_a0 =-45
    step = 0.25
    laser = None

    ## Constructor
    # @param PortName Puerto en el que se encuentra conectado el laser
    # @param Plugged  Se indica si el puerto se encuentra conectado
    # @param i_enable  Indica si el carril izquierdo esta habilitado
    # @param i_range_min  rango minimo asociado al carril izquierdo
    # @param i_range_max  rango maximo asociado al carril izquierdo
    # @param d_enable  Indica si el carril derecho esta habilitado
    # @param d_range_min  rango minimo asociado al carril derecho
    # @param d_range_max  rango maximo asociado al carril derecho
    # @param angle_a0  Angulo  que se forma entre el haz 0 del sensor y el eje x
    def __init__(self,PortName,Plugged = False,i_enable = True,i_range_min = 0,
                    i_range_max = 540,d_enable = True,d_range_min = 541,d_range_max = 1080,angle_a0=-45):
        #super(Hokuyo_utm,self).__init__()
        # definir parametros del sensor
        self.laser =hokuyoaist.Sensor()
        self.PortName = PortName    # nombre del puerto
        self.portParameters='type=serial,device='+self.PortName+',timeout=1'   # parametros del puerto
        self.Data=hokuyoaist.ScanData() # Arreglo donde se almacenan los datos
        self.Plugged = Plugged       # El Sensor esta conectado 
        self.i_enable = i_enable      # El Carril Izquierdo esta habilitado
        self.i_range_min = i_range_min       # Rango minimo del carril Izquierdo
        self.i_range_max = i_range_max     # Rango minimo del carril Izquierdo
        self.d_enable = d_enable       # Carril Derecho Habilitado
        self.d_range_min = d_range_min     # Rango minimo Carril Derecho
        self.d_range_max = d_range_max    # Rango maximo Carril Derecho
        self.angle_a0=angle_a0
        self.portParameters = self.portParameters.encode('ascii')

    
    ## Abre la conexion con el sensor
    def OpenDevice(self):
        print self.portParameters
        self.laser.open( self.portParameters)
        time.sleep(1)
        self.laser.set_power(True)
    
    ## Obtiene un escaneo  del sensor
    def GetRangeScan(self):
        self.laser.get_ranges(self.Data, -1, -1, 1)

    ## Cierra la conexion con el sensor
    def LaserDispose(self):
        if self.laser != None:
            if self.laser.is_open():
                self.laser.close()
    
    ## pregunta si se encuentra la conexion abierta con el sensor
    # @return retorna un booleno  que indica si la conexion esta abierta
    def is_open(self):
        return self.laser.is_open()

    ## obtiene una medida del sensor, y lo concatena con las medidas de tiempo y velocidad asociada
    # @return retorna un arreglo [tiempo, velocidad, escaneo]
    def getRangeScanAndAppend(self,velocity,time):
        self.GetRangeScan()
        x=[]
        x.append(velocity)
        x.append(time)
        for i in range(1081):
           x.append(self.Data.range(i))
        return x


    ##  Crea un diccionario con los parametros del sensor
    # @param Diccionario con los parametros del sensor
    def create_parameters_dictionary(self):
            parameters_dic = dict(self.__dict__)
            del parameters_dic['PortName']
            del parameters_dic['portParameters']
            del parameters_dic['Data']
            del parameters_dic['laser']
            return parameters_dic
    

    
## Clase para la lectura y configuracion del sensor Hokuyo utm 30lx.
# @param laser_scans Tamano del vector  de laser
# @param extraData  Cantidad de datos extra en el vector de laser
# @param SizeVectTemp Cantidad de muestras en el buffer
# @param NoVehiculo  Numero de muestras en las que se ha dejado de detectar vehiculo una vez iniciada el bloque de laser
# @param esperar Indica si  se continua esperando para detectar un vehiculo
# @param data arreglo donde se almacena el bloque de laser 
# @param data_out  bloque de laser con muestra de vehiculo validas
# @param temporal buffer para almacenar muestras previas
# @param contador Contador que permite definir si se debe desplazar el buffer
# @param finalizo Flag que indica si se termino un bloque de laser
# @param vehiculo_medidas Cantidad minima de muestras que debe tener un bloque de laser

class Sample_selection:

    ## Constructor.
    # @param laser_scans Tamano del vector  de laser
    # @param extraData  Cantidad de datos extra en el vector de laser
    # @param SizeVectTemp Cantidad de muestras en el buffer
    def __init__(self, SizeVectTemp=10, laser_scans=(1080/2)+1, extraData=2):
        self.laser_scans = laser_scans
        self.extraData = extraData
        self.SizeVectTemp = SizeVectTemp
        self.NoVehiculo = 0
        self.esperar = True
        self.data = []
        self.data_out = []
        self.temporal = np.zeros(shape=(self.laser_scans+self.extraData,self.SizeVectTemp))
        self.temporalRplidar = [0]*SizeVectTemp
        # print self.temporal.shape, "REISAR1"
        self.contador = 0 
        self.finalizo = False
        self.vehiculo_medidas = 0

    ## Permite Asignar los parametros de configuracion del algoritmo de seleccion de muestras validas
    # @param start  haces de luz por encima del eje x para la seleccion de muestras validas
    # @param stop  haces de luz por debajo del eje x para la seleccion de muestras validas
    # @param step  Cantidad de submuestreminos a realizar en los haces seleccionados
    # @param min_dis distancia minima o radio minimo de la seleccion de muestras validas
    # @param max_dis distancia maxima o radio maximo de la seleccion de muestras validas
    # @param minNoVehicle Cantidad de muestras  de no vehiiculo para cortar un bloque de laser
    # @param minSamples Cantidad de muestras para  considerar un bloque de laser como valido
    def Init_params(self, start=40,stop=110,step=5,min_dis=0.1,max_dis=4.0, minNoVehicle=20, minSamples=50):
        self.start = start
        self.stop = stop
        self.step = step
        self.min_dis = min_dis
        self.max_dis = max_dis
        self.minNoVehicle = minNoVehicle
        self.minSamples = minSamples

    ## Realiza una iteracion de la seleccion de muestras validas
    # @param  x Vector de laser 
    # @param central_angle Angulo central  en el eje x sobre el cual se realiza la seleccion de muestras validas
    # @param start  haces de luz por encima del eje x para la seleccion de muestras validas
    # @param stop  haces de luz por debajo del eje x para la seleccion de muestras validas
    # @param step  Cantidad de submuestreminos a realizar en los haces seleccionados
    # @param min_dis distancia minima o radio minimo de la seleccion de muestras validas
    # @param max_dis distancia maxima o radio maximo de la seleccion de muestras validas
    # @param minNoVehicle Cantidad de muestras  de no vehiiculo para cortar un bloque de laser
    # @param minSamples Cantidad de muestras para  considerar un bloque de laser como valido
    def selection(self, x, central_angle, start=40,stop=110,step=5,min_dis=0.1,max_dis=4.0, minNoVehicle=20, minSamples=50):
        self.desplazar(x)
        central_laser = (central_angle*4)*(-1)
        # print x[360]
        if self.contador>self.SizeVectTemp-1:
            # lee los valores a utilizar para determinar presencia de vehiculo y los convierte a metros
            x = np.asarray(x)
            anglesidx =(central_laser+self.extraData)-np.arange(stop*(-1),start,step,dtype=int)
            # print start,stop, central_laser, "LEEER"
            value_X= x[anglesidx]/1000.0


            # se pregunta si se esta esperando por vehiculo
            # print "vg",anglesidx.shape[0]/10, anglesidx.shape[0],np.sum(np.logical_and(value_X>min_dis,value_X<max_dis))
            # print anglesidx, min_dis, max_dis, start, stop, central_laser, self.extraData
            if (self.esperar):
                # se pregunta si existe presencia de vehiculo, en caso afirmativo se deja de esperar 
                if  np.sum(np.logical_and(value_X>min_dis,value_X<max_dis))>anglesidx.shape[0]/10:
                    self.esperar = False


            else:
                # si hay vehiculo empieza a almacenar muestras y a contar cuantos haz de luz se han almacenado
                self.vehiculo_medidas = self.vehiculo_medidas+1
                self.data.append(self.temporal[:,0])

                #Revisa si deja de haber vehiculo
                if  np.sum(np.logical_or(value_X<min_dis,value_X>max_dis))==anglesidx.shape[0]:
                    self.NoVehiculo = self.NoVehiculo + 1
                    # print "conteo"

                #En el caso de que siga habiedo vehiculo, se resetea el contador NoVehiculo
                else:
                    self.NoVehiculo = 0

                #Si se observan 100 muestras sin vehiculo se deja de guardar
                if (self.NoVehiculo==minNoVehicle):
                    self.NoVehiculo = 0
                    self.esperar = True
                    # print "revisando"
                    #Si se capturan al menos 5 muestras de vehiculo se guarda el bloque final
                    if (self.vehiculo_medidas>=minSamples):
                        self.finalizo = True
                        self.data_out = np.asarray(self.data)
                        
                    self.data=[]
                    self.vehiculo_medidas = 0

    ## desplaza  las muestras dentro del buffer
    # @param x Escaneo del laser a desplazar
    def desplazar(self, x):
        if self.contador<self.SizeVectTemp:
            self.temporal[:,self.contador] = x
            self.contador = self.contador + 1
        else:
            self.temporal = np.concatenate((self.temporal[:,1:self.SizeVectTemp],np.asarray(x).reshape((self.laser_scans+self.extraData,1))),axis=1)
            # print self.temporal[1082/2,9]


    ## Realiza una iteracion de la seleccion de muestras validas para muestras del sensor RPLIDAR
    # @param  x Vector de laser 
    # @param central_angle Angulo central  en el eje x sobre el cual se realiza la seleccion de muestras validas
    # @param inverted permite determinar si se invierte los  valores de start y stop debido a que el angul central se encuentra en el semiplano izquierdo
    def selectionRplidar(self, x, central_angle,inverted=False):
        self.addSampletobuffer(x)
        # se verifica si e n buffer hay suficintes muestras para iniciar el proceso
        if self.contador>self.SizeVectTemp-1:
            # lee los valores a utilizar para determinar presencia de vehiculo y los convierte a metros
            
            angles = x[:,1]
            if inverted:
                anglesidx =np.logical_and(angles >= (central_angle- self.start/4.),angles <= (central_angle + self.stop/4.))
                
            else:
                
                anglesidx =np.logical_and(angles >= (central_angle- self.start/4.),angles <= (central_angle + self.stop/4.))

            value_X= x[anglesidx,2]/1000.0
            if (self.esperar):
                print inverted, central_angle, self.start, self.stop
                if  np.sum(np.logical_and(value_X>self.min_dis,value_X<self.max_dis))>value_X.shape[0]/10:
                    self.esperar = False
            else:
                # si hay vehiculo empieza a almacenar muestras y a contar cuantos haz de luz se han almacenado
                self.vehiculo_medidas = self.vehiculo_medidas+1
                self.data.append(self.temporalRplidar[0])

                #Revisa si deja de haber vehiculo
                if  np.sum(np.logical_or(value_X<self.min_dis,value_X>self.max_dis))==value_X.shape[0]:
                    self.NoVehiculo = self.NoVehiculo + 1
                    # print "conteo"

                #En el caso de que siga habiedo vehiculo, se resetea el contador NoVehiculo
                else:
                    self.NoVehiculo = 0

                #Si se observan 100 muestras sin vehiculo se deja de guardar
                if (self.NoVehiculo==self.minNoVehicle):
                    self.NoVehiculo = 0
                    self.esperar = True
                    # print "revisando"
                    #Si se capturan al menos 5 muestras de vehiculo se guarda el bloque final
                    if (self.vehiculo_medidas>=self.minSamples):
                        self.finalizo = True
                        self.data_out = copy.copy(self.data)
                        
                    self.data=[]
                    self.vehiculo_medidas = 0

    ## desplaza  las muestras dentro del buffer para el sensor RPLIDAR
    # @param x Escaneo del laser a desplazar
    def addSampletobuffer(self,x):
        if self.contador<self.SizeVectTemp:
            self.temporalRplidar[self.contador] = x
            self.contador = self.contador + 1
        else:
            self.temporalRplidar =  self.temporalRplidar[1:]+[x]  