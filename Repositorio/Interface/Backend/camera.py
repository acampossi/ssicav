from __future__ import absolute_import, print_function, division
from pymba import *
import numpy as np
import cv2
import time
import sys
import logging

CARRILES = ["Izquierdo","Derecho"]
AUTO_AJUST = {0:"Off" , 1:"Once" , 2:"Continuous"}

## Objeto que permite la deteccion, apertura, captura de imagenes y cierre de las camaras AlliedVision GIGE
# la clase esta basada  en el API Vimba desarrollada en C  y su respectivo envoltorio para python pymba
# @param camerasIds  Id de las camaras existenes 
# @param Cameras_Dic Diccionario que contiene las instancias de las camaras

class Cameras():

    camerasIds = None
    Cameras_Dic = {}
    Frames = {}
    
    ## Constructor.
    def __init__(self):
        self.RunVimba()
    def RunVimba(self):
        self.vimba = Vimba()
        self.vimba.startup()
        self.system = self.vimba.getSystem()
        
    ## Detecta las camaras conectadas y guarda sus ID en la variable camerasIds
    def detectCameras(self):
        if self.system.GeVTLIsPresent:
            self.system.runFeatureCommand("GeVDiscoveryAllOnce")
            time.sleep(0.2)
        self.cameraIds = self.vimba.getCameraIds()    

    ## Detecta las camaras conectadas, guarda y retorna el valor encontrado
    def GetCamerasId(self):
        self.detectCameras()
        return self.cameraIds

    ## Abre la conexion con todas las camaras disponebles##
    def openCameras(self):
        self.detectCameras()
        for cameraId in self.cameraIds:
            self.OpenCamera(cameraId)
            
    
    ## Abre la conexion con la camara con el id entregado si este existe
    # @param camerasIds  Id de las camara 
    def OpenCamera(self,cameraId):
        if self.cameraIds != None:
            if cameraId in self.cameraIds:
                camera = self.vimba.getCamera(cameraId)
                camera.openCamera()
                camera.PixelFormat = "BGR8Packed"  # OPENCV DEFAULT
                frame = camera.getFrame()
                frame.announceFrame()
                camera.startCapture()
                camera.runFeatureCommand("AcquisitionStart")
                camera.Carril=CARRILES[0]
                self.Cameras_Dic[cameraId]=camera
                self.Frames[cameraId] =frame

    
    ## Retorna la instancia de la camara con el id entregado si este existe
    # @param camerasIds:  Id de las camara 
    # @return instancia de la camara con el id entregado si este existe
    def GetCamera(self,cameraId):
        if self.cameraIds != None:
            if cameraId in self.cameraIds:
                return self.Cameras_Dic[cameraId] 


    ## Obtiene un frame de  la camara con el Id entregado
    # @param camerasIds Id de las camara 
    # @return Retorna un arreglo con un boleano si la captura fue exitosa y el frame obtenido, en caso de contrario entrega NONE  
    def getFrame(self,cameraId):
        img = None
        success = False    
        if cameraId in self.Cameras_Dic:
            #frame = self.Cameras_Dic[cameraId].getFrame()
            self.Frames[cameraId].queueFrameCapture()
            success =True
            # except:
            #     return success,None
            cam = self.Cameras_Dic[cameraId]
            self.Frames[cameraId].waitFrameCapture(1000)
            frame_data = self.Frames[cameraId].getBufferByteData()
            #print (len(frame_data),frame_data[0],cam.Width,cam.Height,cam.OffsetX,cam.OffsetY)
            img = np.ndarray(buffer=frame_data,
                                dtype=np.uint8,
                                shape=(cam.Height, cam.Width, self.Frames[cameraId].pixel_bytes))
            return success,img
        else:
            return success,None
    
    ## Obtiene un frame de  la camara con el Id entregado
    # @param camerasId Id de la camara a configurar
    # @param Values Diccionario que contiene los parametros de configuracion de cada camara de acuerdo a la estructura definida en el JSON de configuracion10
    def ConfigureCamera(self,cameraId,Values):
        if cameraId in self.Cameras_Dic:
            Camera = self.Cameras_Dic[cameraId]
            Camera.Carril = Values["Carril"] 
            Camera.OffsetX = Values["OffsetX"]
            Camera.OffsetY = Values["OffsetY"]
            Camera.Width = Values["Width"]
            Camera.Height = Values["Height"]
            Camera.ExposureAuto = Values["ExposureAuto"]
            Camera.ExposureAutoTarget = Values["ExposureAutoTarget"]
            Camera.GainAuto = Values["GainAuto"]
            Camera.GainAutoTarget = Values["GainAutoTarget"]
            Camera.BalanceWhiteAuto = Values["BalanceWhiteAuto"]
            Camera.BalanceWhiteAutoRate = Values["BalanceWhiteAutoRate"]
         
    ## Configura el modo de ajuste de la exposicion de la camara seleccionada
    # @param cameraId Id de la camara a configurar
    # @param value  valor a definir para el ajuste entre 0 y 2: 0:"Off" , 1:"Once" , 2:"Continuous"
    def SetCameraExposure(self,cameraId,value):
        if value in AUTO_AJUST:
            if cameraId in self.Cameras_Dic:
                self.Cameras_Dic[cameraId].ExposureAuto= AUTO_AJUST[value]

    ## Obtiene  el modo de ajuste de la exposicion de la camara seleccionada
    # @param cameraId Id de la camara a configurar
    # @return  valor del ajuste entre 0 y 2:  0:"Off" , 1:"Once" , 2:"Continuous"
    def GetCameraExposure(self,cameraId):
        if cameraId in self.Cameras_Dic:
            return AUTO_AJUST.keys()[AUTO_AJUST.values().index(self.Cameras_Dic[cameraId].ExposureAuto)]

    ## Configura el modo de ajuste de la ganancia de la camara seleccionada
    # @param cameraId Id de la camara a configurar
    # @param value  valor a definir para el ajuste entre 0 y 2: 0:"Off" , 1:"Once" , 2:"Continuous"
    def SetCameraGain(self,cameraId,value):
        if value in AUTO_AJUST:
            if cameraId in self.Cameras_Dic:
                self.Cameras_Dic[cameraId].GainAuto= AUTO_AJUST[value]
    
    ## Obtiene  el modo de ajuste de la Ganancia de la camara seleccionada
    # @param cameraId Id de la camara a configurar
    # @return  valor del ajuste entre 0 y 2:  0:"Off" , 1:"Once" , 2:"Continuous"
    def GetCameraGain(self,cameraId):
        if cameraId in self.Cameras_Dic:
            return AUTO_AJUST.keys()[AUTO_AJUST.values().index(self.Cameras_Dic[cameraId].GainAuto)]

    ## Configura el modo de ajuste del balance de blancos de la camara seleccionada
    # @param cameraId Id de la camara a configurar
    # @param value  valor a definir para el ajuste entre 0 y 2: 0:"Off" , 1:"Once" , 2:"Continuous"
    def SetCameraBalanceWhite(self,cameraId,value):
        if value in AUTO_AJUST:
            if cameraId in self.Cameras_Dic:
                self.Cameras_Dic[cameraId].BalanceWhiteAutoAuto= AUTO_AJUST[value]

    ## Obtiene  el modo de ajuste del balance de blancos de la camara seleccionada
    # @param cameraId Id de la camara a configurar
    # @return  valor del ajuste entre 0 y 2:  0:"Off" , 1:"Once" , 2:"Continuous"
    def GetCameraBalanceWhite(self,cameraId):
        if cameraId in self.Cameras_Dic:
            return AUTO_AJUST.keys()[AUTO_AJUST.values().index(self.Cameras_Dic[cameraId].BalanceWhiteAuto)]

    
    ##Cierra la conexion con una camara
    # @param cameraId Id de las camara 
    def DisposeCamera(self,cameraId):
        if cameraId in self.Cameras_Dic:
            self.Cameras_Dic[cameraId].runFeatureCommand("AcquisitionStop")
            self.Cameras_Dic[cameraId].endCapture()
            self.Cameras_Dic[cameraId].revokeAllFrames()
            del self.cameraIds[cameraId]

    ## Cierra la conexion con todas camaras  y cierra la conexion con vimba
    def Dispose(self):
        for key in self.Cameras_Dic:
            self.Cameras_Dic[key].runFeatureCommand("AcquisitionStop")
            self.Cameras_Dic[key].endCapture()
            self.Cameras_Dic[key].revokeAllFrames()
        self.vimba.shutdown()
   
    ## Entrega la lista de posibles carriles
    # @return Listado de carriles
    def getLaneListType(self):
        return CARRILES

    ## Entrega el tipo de ajuste
    # @return Entrega el  valor asocioado a cada tipo de ajuste
    def GetAutoAdjustIndex(self,value):
        return AUTO_AJUST.keys()[AUTO_AJUST.values().index(value)]

#############################################################################
#___________________________________________________________________________#
#############################################################################
if __name__ == "__main__":
    cv2.namedWindow("test")
    Cams =Cameras()
    Cams.openCameras()
    count = 1
    while 1:
        success, img = Cams.getFrame(Cams.cameraIds[0])
        if success:
            cv2.imshow("test", img)
            k = cv2.waitKey(1)
            if k == 0x1b:
                cv2.destroyAllWindows()
                break
            elif k%256 == 32:
                cv2.imwrite("image"+str(count)+".png", img)
                count = count+1
    Cams.Dispose()
    
"""
    # with Vimba() as vimba:
    #     system = vimba.getSystem()

    #     system.runFeatureCommand("GeVDiscoveryAllOnce")
    #     time.sleep(0.2)

    #     camera_ids = vimba.getCameraIds()

    #     for cam_id in camera_ids:
    #         print("Camera found: ", cam_id)

    #     c0 = vimba.getCamera(camera_ids[0])
    #     c0.openCamera()



    #     frame = c0.getFrame()
    #     frame.announceFrame()

    #     c0.startCapture()
    #     c0.runFeatureCommand("AcquisitionStart")

    #     framecount = 0
    #     droppedframes = []

    #     while 1:
    #         try:
    #             frame.queueFrameCapture()
    #             success = True
    #         except:
    #             droppedframes.append(framecount)
    #             success = False
    #         #c0.runFeatureCommand("AcquisitionStop")
    #         frame.waitFrameCapture(1000)
    #         frame_data = frame.getBufferByteData()
    #         if success:
    #             img = np.ndarray(buffer=frame_data,
    #                             dtype=np.uint8,
    #                             shape=(frame.height, frame.width, frame.pixel_bytes))
    #             cv2.imshow("test", img)
    #         framecount += 1
    #         k = cv2.waitKey(1)
    #         if k == 0x1b:
    #             cv2.destroyAllWindows()
    #             print("Frames displayed: %i" % framecount)
    #             print("Frames dropped: %s" % droppedframes)
    #             break


    #     c0.endCapture()
    #     c0.revokeAllFrames()
 
    #     c0.closeCamera()"""