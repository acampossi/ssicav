#include <iostream>
#include <pcl/ModelCoefficients.h>
#include <pcl/point_types.h>
#include <pcl/io/pcd_io.h>
#include <pcl/filters/extract_indices.h>
#include <pcl/filters/voxel_grid.h>
#include <pcl/features/normal_3d.h>
#include <pcl/kdtree/kdtree.h>
#include <pcl/sample_consensus/method_types.h>
#include <pcl/sample_consensus/model_types.h>
#include <pcl/segmentation/sac_segmentation.h>
#include <pcl/segmentation/extract_clusters.h>
#include <pcl/console/parse.h>
#include <pcl/common/common_headers.h>
#include <pcl/filters/statistical_outlier_removal.h>

#include <pcl/common/transforms.h>
#include <boost/thread/thread.hpp>
#include <pcl/range_image/range_image.h>
#include <pcl/visualization/range_image_visualizer.h>
#include <pcl/visualization/pcl_visualizer.h>

#include "FPFH_VFH_Classes.h"
#include "Cluster.h"


#include <boost/python.hpp>
#include "boost/python/extract.hpp"
#include "boost/python/numeric.hpp"
using namespace boost::python;

void PassCloudClasses(Cluster& self1, VFH_FPFH_C& self2, std::string cloudS ){
    self1.itx = self1.my_map.find(cloudS);
    if (self1.itx != self1.my_map.end()){
        self2.basic_cloud_ptr->clear();
        *self2.basic_cloud_ptr = self1.my_map[cloudS];
    }
    else{
        PCL_ERROR ("No existe la Nube de entrada  \n");
    }


}

// void Normals_visual(Cluster& self1, std::string cloudS, float radiusS ){
//     pcl::PointCloud<pcl::PointXYZ>::Ptr basic_cloud_ptr(new pcl::PointCloud<pcl::PointXYZ>) ;
//     pcl::PointCloud<pcl::Normal> cloud_normals2;
//     self1.itx = self1.my_map.find(cloudS);
//     if (self1.itx != self1.my_map.end()){
//         *basic_cloud_ptr = self1.my_map[cloudS];
//     }
//     else{
//         PCL_ERROR ("No existe la Nube de entrada  \n");
//     }
//     pcl::NormalEstimationOMP<pcl::PointXYZ, pcl::Normal> ne;

//     ne.setInputCloud (basic_cloud_ptr);
//     pcl::search::KdTree<pcl::PointXYZ>::Ptr tree (new pcl::search::KdTree<pcl::PointXYZ> ());

//     ne.setSearchMethod (tree);
//     // cout << "radio busqueda "<<radiusS<<".\n";
//     ne.setRadiusSearch (radiusS);
//     // -- remueve las normales Nan de la nube de puntos
//     std::vector<int> indicesz;
//     pcl::removeNaNNormalsFromPointCloud(cloud_normals2,cloud_normals2,indicesz);
//     pcl::copyPointCloud(*basic_cloud_ptr,indicesz,*basic_cloud_ptr);
    
// }

BOOST_PYTHON_MODULE(bridge_fpfh_vfh)
{
	boost::python::numeric::array::set_module_and_type("numpy", "ndarray");
    class_<Cluster>("Cluster")
        .def("readCloud", &Cluster::readCloud)
        .def("passCloud", &Cluster::passCloud)
        .def("GetIndices", &Cluster::GetIndices)
        .def("GetIndicesGlobal_B", &Cluster::GetIndicesGlobal_B)
        .def("GetIndicesGlobal", &Cluster::GetIndicesGlobal)
        .def("GetIndicesLocal", &Cluster::GetIndicesLocal)
        .def("GetCluster", &Cluster::GetCluster)
        .def("GetClusterGlobal", &Cluster::GetClusterGlobal)
        .def("GetClusterLocal", &Cluster::GetClusterLocal)
        .def("filter_outliers", &Cluster::filter_outliers)
        .def("filter_outliers_B", &Cluster::filter_outliers_B)
        .def("DemeanCluster", &Cluster::DemeanCluster)
        .def("Angle_Rotate", &Cluster::Angle_Rotate)
        .def("saveCluster", &Cluster::saveCluster)
        .def("CountWheels", &Cluster::CountWheels)
        .def("CountWheels_B", &Cluster::CountWheels_B)
        .def("getSizeClusterGlobal", &Cluster::getSizeClusterGlobal)
        .def("getSizeClusterLocal", &Cluster::getSizeClusterLocal)
        .def("CalculateHeight", &Cluster::CalculateHeight)
        .def("CalculateHeightFirstEje", &Cluster::CalculateHeightFirstEje)
        .def("CalculateMinMax", &Cluster::CalculateMinMax) 
        .def("passtroughtFilter", &Cluster::passtroughtFilter)
        .def("set_Parameters_ALL", &Cluster::set_Parameters_ALL)
        .def("getCloudSize", &Cluster::getCloudSize)
        .def("getClusterCloud", &Cluster::getClusterCloud)
        .def("clearMymap", &Cluster::clearMymap)

        .add_property("Cluster_global_min", &Cluster::get_Cluster_global_min, &Cluster::set_Cluster_global_min)   
        .add_property("Cluster_global_max", &Cluster::get_Cluster_global_max, &Cluster::set_Cluster_global_max)   
        .add_property("Cluster_global_tolerance", &Cluster::get_Cluster_global_tolerance, &Cluster::set_Cluster_global_tolerance)   
        .add_property("Filter_outliers_mean", &Cluster::get_Filter_outliers_mean, &Cluster::set_Filter_outliers_mean)   
        .add_property("Filter_outliers_Thresh", &Cluster::get_Filter_outliers_Thresh, &Cluster::set_Filter_outliers_Thresh)   
        .add_property("Wheels_tolerance", &Cluster::get_Wheels_tolerance, &Cluster::set_Wheels_tolerance)   
        .add_property("Wheels_size_min", &Cluster::get_Wheels_size_min, &Cluster::set_Wheels_size_min)   
        .add_property("Wheels_size_max", &Cluster::get_Wheels_size_max, &Cluster::set_Wheels_size_max)   
        .add_property("Wheels_delta_ground", &Cluster::get_Wheels_delta_ground, &Cluster::set_Wheels_delta_ground)  
        .add_property("Wheels_distance_filter", &Cluster::get_Wheels_distance_filter, &Cluster::set_Wheels_distance_filter)  
        .add_property("Wheels_min_width", &Cluster::get_Wheels_min_width, &Cluster::set_Wheels_min_width)
    ;

    class_<VFH_FPFH_C>("VFH_FPFH_C")
        .def("readCloud", &VFH_FPFH_C::readCloud)
        .def("passCloud", &VFH_FPFH_C::passCloud)
        .def("CalculatePointDensity", &VFH_FPFH_C::CalculatePointDensity)
        .def("CalculateNormals", &VFH_FPFH_C::CalculateNormals)
        .def("CalculateNormals_V", &VFH_FPFH_C::CalculateNormals_V)
        .def("UniformSampling", &VFH_FPFH_C::UniformSampling)
        .def("CalculateFPFHwithoutSampling", &VFH_FPFH_C::CalculateFPFHwithoutSampling)
        .def("CalculateFPFHSampling", &VFH_FPFH_C::CalculateFPFHSampling)
        .def("CalculatePointDensity_B", &VFH_FPFH_C::CalculatePointDensity_B)
        .def("CalculateNormals_B", &VFH_FPFH_C::CalculateNormals_B)
        .def("UniformSampling_B", &VFH_FPFH_C::UniformSampling_B)
        .def("CalculateFPFHwithoutSampling_B", &VFH_FPFH_C::CalculateFPFHwithoutSampling_B)
        .def("CalculateFPFHSampling_B", &VFH_FPFH_C::CalculateFPFHSampling_B)
        .def("Appendo", &VFH_FPFH_C::Appendo)
        .def("saveFinal", &VFH_FPFH_C::saveFinal)
        .def("putWidth", &VFH_FPFH_C::putWidth)
        .def("getValueFPFH", &VFH_FPFH_C::getValueFPFH)
        .def("save", &VFH_FPFH_C::save)
        .def("ClearValues", &VFH_FPFH_C::ClearValues)
        .def("normalsVis", &VFH_FPFH_C::normalsVis)
        .def("InitViewer", &VFH_FPFH_C::InitViewer)
        .def("CloseVis", &VFH_FPFH_C::CloseVis)
        .def("set_Parameters_ALL", &VFH_FPFH_C::set_Parameters_ALL)
        .def("CalculateVFHwithoutSampling", &VFH_FPFH_C::CalculateVFHwithoutSampling)
        .def("CalculateVFHSampling", &VFH_FPFH_C::CalculateVFHSampling)
        .def("VFH_Save", &VFH_FPFH_C::VFH_Save)
        .def("getValueVFH", &VFH_FPFH_C::getValueVFH)
        .def("CalculateVFHwithoutSampling_B", &VFH_FPFH_C::CalculateVFHwithoutSampling_B)
        .def("CalculateVFHSampling_B", &VFH_FPFH_C::CalculateVFHSampling_B)
        .def("Save_point_normals", &VFH_FPFH_C::Save_point_normals)
        .def("Get_point_normals", &VFH_FPFH_C::Get_point_normals)
        .def("getCloudSize", &VFH_FPFH_C::getCloudSize)

        .add_property("Density_delta", &VFH_FPFH_C::get_Density_delta, &VFH_FPFH_C::set_Density_delta)   
        .add_property("Normals_radiusS", &VFH_FPFH_C::get_Normals_radiusS, &VFH_FPFH_C::set_Normals_radiusS)   
        .add_property("USampling_radiusS", &VFH_FPFH_C::get_USampling_radiusS, &VFH_FPFH_C::set_USampling_radiusS)   
        .add_property("FPFH_radiusS", &VFH_FPFH_C::get_FPFH_radiusS, &VFH_FPFH_C::set_FPFH_radiusS)   
        .add_property("VFH_radiusS", &VFH_FPFH_C::get_VFH_radiusS, &VFH_FPFH_C::set_VFH_radiusS) 
    ;
    def("PassCloudClasses", &PassCloudClasses);
}