#!/usr/bin/env python
# -*- coding: utf-8 -*-
import sys
from  PyQt4 import QtGui,QtCore
import numpy as np
import os

TABS_BASEDIR = os.path.dirname(os.path.abspath(__file__))
sys.path.insert(0, TABS_BASEDIR + './VisualizationWidgets')
from VisualizationWidgets.VisualizationWidgets import *

sys.path.insert(0, TABS_BASEDIR +'./Tabs')

from Tabs.Hokuyotab import LaserHarwtab
from Tabs.Radartab import RadarHarwtab
from Tabs.Prosilicatab import ProsilicaHarwtab
from Tabs.DistanceFilter import DisFilertab
from Tabs.Clasification import ClasificationTab
from Tabs.LicensePlatetab import LPtab
from Tabs.SamplesSelection import SamplesSelectionTab

class Software_Tabs(QtGui.QTabWidget):
    
    def __init__(self, parent = None,Prototipe = False):
        super(Software_Tabs, self).__init__(parent)
        self.prototipe = Prototipe        
        self.CreateInterface()        


    def CreateInterface(self):
        self.setWindowTitle(u"AFORO DE VEHÍCULOS - SSI - Configurar Software...")
        stylesheet = """ 
        QTabBar::tab:selected {background: rgb(33,150,243);color:white; border-color:black}
        
        """
        self.setTabShape(0)
        self.setStyleSheet(stylesheet)
        self.SamplesSelectionTab =SamplesSelectionTab()
        self.DistanceFilter = DisFilertab()
        self.Clasification = ClasificationTab()
        self.LPtab = LPtab()
        self.addTab(self.SamplesSelectionTab,u"Muestras Validas")
        self.SamplesSelectionTab_index =0
        self.addTab(self.DistanceFilter,u"Filtrado de distancia")
        self.DistanceFilter_index =1
        self.addTab(self.Clasification,u"Clasificación")
        self.Clasification_index =2
        if  not (self.prototipe):
            
            pass#self.addTab(self.LPtab,u"Detección de placas")
        self.LPtab_index = 3

class Harware_Tabs(QtGui.QTabWidget):

    def __init__(self, parent = None, Prototipe=False):
        super(Harware_Tabs, self).__init__(parent)
        self.prototipe = Prototipe
        # print Prototipe,self.prototipe,"JK"
        self.CreateInterface()        
        
    def CreateInterface(self):
        self.setWindowTitle(u"AFORO DE VEHÍCULOS - SSI - Configurar Hardware...")
        stylesheet = """ 
        QTabBar::tab:selected {background: rgb(33,150,243);color:white; border-color:black}
        
        """
        self.setTabShape(0)
        self.setStyleSheet(stylesheet)
        self.ProsilicaHarwtab = ProsilicaHarwtab()
        self.LaserHarwtab = LaserHarwtab()
        self.RadarHarwtab = RadarHarwtab()
        self.addTab(self.LaserHarwtab,"Laser")
        self.LaserHarwtab_index =0
        print self.prototipe,"aqui"
        if not (self.prototipe):
            self.addTab(self.RadarHarwtab,"Radar")
            self.addTab(self.ProsilicaHarwtab,"Camara")
        self.RadarHarwtab_index =1
        self.ProsilicaHarwtab_index = 2
            
if __name__ == '__main__':
   app = QtGui.QApplication(sys.argv)
   ex = Harware_Tabs()
   ex.show()
   sys.exit(app.exec_())
	