# -*- coding: utf-8 -*-
# vispy: gallery 30
# -----------------------------------------------------------------------------
# Copyright (c) 2015, Vispy Development Team. All Rights Reserved.
# Distributed under the (new) BSD License. See LICENSE.txt for more info.
# -----------------------------------------------------------------------------
"""
Test automatic layout of multiple viewboxes using Grid.
"""
import sys
import numpy as np

from vispy import scene, app

canvas = scene.SceneCanvas(keys='interactive', size=(600, 600), show=True,bgcolor='white')
grid = canvas.central_widget.add_grid(margin=10)
grid.spacing = 0

title = scene.Label("Plot Title", color='black')
title.height_max = 40
grid.add_widget(title, row=0, col=0, col_span=2)

grid1 = scene.GridLines(parent =grid)

yaxis = scene.AxisWidget(orientation='left',
                         axis_label='Y Axis',
                         axis_font_size=12,
                         axis_label_margin=50,
                         tick_label_margin=5,
                         text_color='black')
yaxis.width_max = 80
grid.add_widget(yaxis, row=1, col=0)

xaxis = scene.AxisWidget(orientation='bottom',
                         axis_label='X Axis',
                         axis_font_size=12,
                         axis_label_margin=50,
                         tick_label_margin=5,
                         text_color='black')

xaxis.height_max = 80
grid.add_widget(xaxis, row=2, col=1)

right_padding = grid.add_widget(row=1, col=2, row_span=1)
right_padding.width_max = 50

view = grid.add_view(row=1, col=1, border_color='black')

data = np.arange(10)#.reshape((5,2))
plot = scene.LinePlot(data, parent=view.scene)
plot.set_data(data)
view.camera = 'panzoom'

xaxis.link_view(view)
yaxis.link_view(view)



if __name__ == '__main__' and sys.flags.interactive == 0:
    app.run()
