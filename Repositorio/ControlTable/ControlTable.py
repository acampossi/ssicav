import sqlite3
import time
import multiprocessing
import sys, select
# pylint: disable=line-too-long

class DBControl:
    """Modulo de registro y control de vehiculos del sistema"""
    def __init__(self, db_name=':memory:', temp_table_name='TempReg', def_table_name='DefReg',debug=0):
        """Crea un objeto DBcontrol

        Keyword arguments:
        db_name --  nombre del archivo .db a utilizar, por defecto table.db
        temp_table_name -- nombre de la tabla temporal para el almacenamiento de registros sin procesar
        def_table_name -- nombre de la tabla de registro local para envio al servidor
        """
        # definir si se desea mostrar avisos en la ejecucion del script
        self.debug = debug
        # nombre del archivo .db
        self.db_name = db_name
        # nombre de la tabla de registro local
        self.def_table_name = def_table_name
        # nombre de la tabla de control de hilos
        self.temp_table_name = temp_table_name
        # conexion con la base de datos
        self.conexion = sqlite3.connect(self.db_name)
        # crear consulta con la base de datos
        self.consulta = self.conexion.cursor()
        # definir el modo de entrega de datos por parte de la tabla
        self.conexion.row_factory = sqlite3.Row
        # definir la estructura de la  tabla
        self.table = "CREATE TABLE "+self.temp_table_name+" (id INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL,"\
                " NubePuntos VARCHAR(255) NOT NULL, "\
                " NubePrep VARCHAR(255), "\
                " Caracteristicas VARCHAR(255), "\
                " Fecha DATE NOT NULL, "\
                " Tiempo_Inicial TIME NOT NULL, "\
                " Tiempo_Final TIME NOT NULL, "\
                " Placa_Veh VARCHAR(255),"\
                " V_Foto_Ruta VARCHAR(255),"\
                " Carril INTEGER(9) NOT NULL,"\
                " Sentido VARCHAR(255) NOT NULL,"\
                " Clase INTEGER(9) NOT NULL,"\
                " Esta_Preprocesado INTEGER(9) NOT NULL,"\
                " Esta_Segmentado INTEGER(9) NOT NULL,"\
                " Esta_Clasificado  INTEGER(9) NOT NULL,"\
                " Esta_Registrado  INTEGER(9) NOT NULL);"
        # interar crear la tabla con la estructura definida
        try:
            self.consulta.execute(self.table)
            self.conexion.commit()
            print "La tabla fue creada..."
        except:
            print "La tabla ya esta creada"



    def crear_tabla_local(self):
        """Crear la tabla de registro local"""
        # Definir la estructura de la tabla
        table = "CREATE TABLE FinalReg (id INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL,"\
                " Fecha DATE NOT NULL, "\
                " Tiempo_Inicial TIME NOT NULL, "\
                " Carril VARCHAR(255) NOT NULL,"\
                " Sentido VARCHAR(255) NOT NULL,"\
                " Clase INTEGER(9) NOT NULL,"\
                " Placa_Veh VARCHAR(255) NOT NULL,"\
                " V_Foto_Ruta VARCHAR(255) NOT NULL,"\
                " Color_placa VARCHAR(255) NOT NULL);"
        # interar crear la tabla con la estructura definida
        try:
            self.consulta.execute(table)
            self.conexion.commit()
            print "La tabla fue creada exitosamente!"
        except:
            print "La tabla ya existe o no ha podido ser creada!"



    def insertar_nuevo_registro(self, path, Tiempo_Inicial, Tiempo_Final, Carril, Sentido):
        """Crear un nuevo registro de nube de puntos para que sea procesada por los otros hilos"""
        #" NombreClusters, "\
        # " Placa_Veh,"\
        #" V_Foto_Ruta,"\

        # construir el  query de sqlite3 para realizar la insercion del nuevo archivo
        insert = "insert into "+self.temp_table_name+ " ("\
                " NubePuntos, "\
                " Fecha, "\
                " Tiempo_Inicial, "\
                " Tiempo_Final, "\
                " Carril,"\
                " Sentido,"\
                " Clase,"\
                " Esta_Preprocesado,"\
                " Esta_Segmentado,"\
                " Esta_Clasificado,"\
                " Esta_Registrado) values ('"\
                +path+"' , '"\
                +time.strftime("%y-%m-%d")+"' , '"\
                +Tiempo_Inicial+"' , '"\
                +Tiempo_Final+"' , "\
                +str(Carril)+" , '"\
                +Sentido+"' , "\
                +"-1 , 0 , 0 , 0, 0)"
        #se dispone todas las banderas en cero para el nuevo registro sea procesado por completo


        if self.consulta.execute(insert):
            print "El registro fue insertado..."
        else:
            print "el registro no fue insertado..."
        self.conexion.commit()

    def Dispose(self):
        self.consulta.close()
        self.consulta .close()


    def consultar_tabla_temporal(self, campos=[]):
        if campos == []:
            select = "select * from " + self.temp_table_name
            self.consulta.execute(select)
            resultado_consulta = self.consulta.fetchall()
        else:
            select = "select "
            for item in campos:
                select = select + item+" , "
            select = select[:-3] + " from " + self.temp_table_name
            self.consulta.execute(select)
            resultado_consulta = self.consulta.fetchall()

        return resultado_consulta

    def imprimir_tablas_en_terminal(self):
        pass




def hilo1obt(db_obj, q):


    db_obj.insertar_nuevo_registro('a',	time.strftime('%H:%M:%S'), time.strftime('%H:%M:%S'), 0, 'norte-sur')
    db_obj.insertar_nuevo_registro('a',	time.strftime('%H:%M:%S'), time.strftime('%H:%M:%S'), 0, 'norte-sur')

def hilo2obt(db_obj):
    
    start = time.time()
    for i in range(100):
        db_obj.insertar_nuevo_registro('b',	time.strftime('%H:%M:%S'), time.strftime('%H:%M:%S'), 0, 'norte-sur')
        db_obj.insertar_nuevo_registro('b',	time.strftime('%H:%M:%S'), time.strftime('%H:%M:%S'), 0, 'norte-sur')
    end = time.time()
    print(end - start)


db_obj=DBControl()
#db_obj.createFileTable()
db_obj.insertar_nuevo_registro('a',	time.strftime('%H:%M:%S'), time.strftime('%H:%M:%S'), 0, 'norte-sur')
db_obj.insertar_nuevo_registro('b',	time.strftime('%H:%M:%S'), time.strftime('%H:%M:%S'), 0, 'norte-sur')
a = db_obj.consultar_tabla_temporal(campos=['NubePuntos', 'Esta_Preprocesado'])
print a
#db_obj.Dispose()


try:
#    hilo1 = multiprocessing.Process(name='hilo1', target=hilo1obt, args=(db_obj,))
    hilo2 = multiprocessing.Process(name='hilo2', target=hilo2obt, args=(db_obj,))
#    hilo1.start()
    hilo2.start()
except:
   print "Error: unable to start thread"


while 1:
    if sys.stdin in select.select([sys.stdin], [], [], 0)[0]:
        line = raw_input()
    	a= db_obj.consultar_tabla_temporal(campos = ['NubePuntos', 'Esta_Preprocesado'])
        print a
        db_obj.Dispose()
        hilo2.join()
        break
