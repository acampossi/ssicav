function [a,med]=GaussianModelStimation(a,thd)
if (a.SamplesCount>30)
    Result=and(a.mean-2*a.std<=a.New,a.mean+2*a.std>=a.New);
    med=sum(Result(:))/length(Result);
    if (med>thd*3)
        a.SamplesCount=a.SamplesCount+1;
        a.LastPos=a.LastPos+1;    
        if (a.LastPos>a.limit)
            a.LastPos=1;
        end
        if (a.SamplesCount<=a.limit)
            [a.mean,a.Dev]=MeanAndDesviation(a.mean,a.Dev,a.New,a.SamplesCount);
        else
            [a.mean,a.Dev]=SlidingMeanAndDesviation(a.mean,a.Dev,a.New,a.Samples(:,a.LastPos,:),a.limit);
        end
        a.std=sqrt(a.Dev);
        a.Samples(:,a.LastPos,:)=a.New;
    end
else
    [a.mean,a.Dev]=MeanAndDesviation(a.mean,a.Dev,a.New,a.SamplesCount);
    a.std=sqrt(a.Dev);
    a.SamplesCount=a.SamplesCount+1;
    a.LastPos=a.LastPos+1;    
    if (a.LastPos>a.limit)
        a.LastPos=1;
    end
    a.Samples(:,a.LastPos,:)=a.New;
    med=-1;
end


end