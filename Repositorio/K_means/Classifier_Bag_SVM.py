import os
import sys
import numpy as np
from pylab import ginput
import matplotlib.pyplot as plt
import matplotlib.image as mpimg
import math
import time
from sklearn.cluster import MiniBatchKMeans, KMeans
from sklearn.decomposition import PCA
from sklearn import svm
from sklearn.metrics import confusion_matrix
import itertools
from sklearn.externals import joblib
from sklearn.metrics import accuracy_score
from sklearn.model_selection import train_test_split
from sklearn import (manifold, datasets, decomposition, ensemble,
                     discriminant_analysis, random_projection)
# pylint: disable=line-too-long
# -*- coding: utf-8 -*-
# -*- coding: 850 -*-

class Vehicle_Category_Classifier:
    """
    la clase Vehicle_Category_Classifier  permite entrenar, evaluar, guardar  o cargar
    modelos para la clasificacion basada en bolsa de caracteristicas mediante K means
    y maquinas de soporte vectorial
    """
    def __init__(self, n_clusters=20, n_classes=2):
        """ Crea una nuevo objeto de clasificacion  basado en maquinas de soporte vectorial  y k-means
        Argumentos:
        n_clusters -- numero de clusters a crear para el entrenamiento de k means, por defecto 20
        n_classes --  numero de clases a crear, por defecto 2
        """
        self.n_Clusters = n_clusters #numero de clusters del clasificador
        self.n_Classes = n_classes   #numero de clases del clasificadoor
        self.k_means_dic = {}
        self.SVM_dic = {}
        self.histogram_index = range(self.n_Clusters+1)

    # Entrenar Kmeans  y SVMS con todas las combinaciones posibles
    def train(self, Vehicle_Descriptors, descriptors_labels, vehicle_labels, class_weight=None):
        """Entrena k means con centroides seleccionados usando: k-means++, PCA y Seleccion aleatoria.
        Posteriormente entrena maquinas de soporte vectorial RBF,Lineales y polinomiales y las almacena en diccionarios para ser evaluados
        Argumentos:
        Vehicle_Descriptors -- arreglo con los descriptores de los vehiculos el cual debe tener dimensiones
        [sum(descriptors_labels), Numero_caracteristicas_descriptor]
        descriptors_labels --  arreglo tipo columna con el numero de descriptores por cada vehiculo
        Vehicle_Descriptors -- arreglo tipo columna con las etiquetas de los vehiculos a utilizar para entrenar
        class_weight -- se definen que ponderacion se le da a las clases en el momento de clasificar los cuales seran
        por defecto None, los pesos se entregan como un diccionario con el nombre de la clase y el peso
        """
        # entrenar los kmeans mediante diferentes metodos
        self.trainKmeans(Vehicle_Descriptors)
        # por cada kmeans se realiza el entrenamiento de los svms
        self.trainsvms(Vehicle_Descriptors, descriptors_labels, vehicle_labels, class_weight=None)

    # Entrenar Kmeans  y SVMS con todas las combinaciones posibles
    def trainsvms(self, Vehicle_Descriptors, descriptors_labels, vehicle_labels, class_weight=None):
        """Entrenar maquinas de soporte vectorial con todas las combinaciones posibles
        Argumentos:
        Vehicle_Descriptors -- arreglo con los descriptores de los vehiculos el cual debe tener dimensiones
        [sum(descriptors_labels), Numero_caracteristicas_descriptor]
        descriptors_labels --  arreglo tipo columna con el numero de descriptores por cada vehiculo
        vehicle_labels -- arreglo tipo columna con las etiquetas de los vehiculos a utilizar para entrenar
        class_weight -- se definen que ponderacion se le da a las clases en el momento de clasificar los cuales seran
        por defecto None, los pesos se entregan como un diccionario con el nombre de la clase y el peso
        """
        # por cada kmeans se realiza el entrenamiento de los svms
        for key, kmean in self.k_means_dic.iteritems():
            # se evaluan los datos para cada  K-means  y se generan los histogramas
            svm_input = self.EvaluateKmeans_CreateHistograms(kmean, Vehicle_Descriptors, descriptors_labels)
            # Se entrenan los SVM con los histogramas generados
            self.train_svm_kmeans_selected(key, svm_input, vehicle_labels, class_weight=class_weight)



    # Entrenar maquinas de vectores de soporte
    def train_svm_kmeans_selected(self, key, input_data, labels, C=1, Gamma=0.05, Degree=3, class_weight=None):
        """ Entrena maquinas de soporte vectorial con  los histogramas obtenidos mediante k means ya decodificados por k means
        Argumentos:
        input_data -- Histogramas obtenidos mediante la evaluacion de K -means
        labels -- Etiquetas de los vehiculos a clasificar
        C --     Penalizacion maquinas de soporte vectorial para datasets desvalanceados
        Gamma -- ancho de la gaussiana para la maquina de soporte vectorial con kernel de base radial, por defecto 0.7
        Degree -- Grado del polinomio para la maquina de soporte vectorial con kernel polinomial
        class_weight -- se definen que ponderacion se le da a las clases en el momento de clasificar los cuales seran
        por defecto None, los pesos se entregan como un diccionario con el nombre de la clase y el peso
        """
        print "Entrenando clasificadores..."
        # Se entrenan SVMs con distintas topologias:
        # lineal...
        self.SVM_dic[(key, 'lin')] = svm.SVC(kernel='linear', C=C, class_weight=class_weight).fit(input_data, labels)
        # rbf...
        self.SVM_dic[(key, 'rbf')] = svm.SVC(kernel='rbf', gamma=Gamma, C=C, class_weight=class_weight).fit(input_data, labels)
        # Polinomial
        self.SVM_dic[(key, 'poly')] = svm.SVC(kernel='poly', degree=Degree, C=C, class_weight=class_weight).fit(input_data, labels)
        # lineal segunda topologia
        self.SVM_dic[(key, 'lin-SVC')] = svm.LinearSVC(C=C, class_weight=class_weight).fit(input_data, labels)
        print "Finalizado.."


    # #entrenar K means
    def trainKmeans(self, Vehicle_Descriptors):
        """ Entrena modelos de K means utilizando los descriptores extraidos a los vehiculos a clasificar
        Argumentos:
        Vehicle_Descriptors -- matriz con descriptores de los vehiculos  de dimensiones [numero_descriptores,dimensiones_descritor]
        """
        batch_size = 64
        print "Entrenando Kmeans..."
        # Entrenar  con centroides iniciales definidos mediante K-means++
        self.k_means_dic['k-means++'] = MiniBatchKMeans(init='k-means++', n_clusters=self.n_Clusters, n_init=10, batch_size=batch_size, max_no_improvement=10, verbose=0).fit(Vehicle_Descriptors)
        #self.k_means_dic['k-means++'] = KMeans(init='k-means++', n_clusters=self.n_Clusters, n_init=10, max_iter=2000).fit(Vehicle_Descriptors)
        # Entrenar  con centroides iniciales definidos aleatoriamente con 10 intentos
        self.k_means_dic['random'] = MiniBatchKMeans(init='random', n_clusters=self.n_Clusters, n_init=10, batch_size=batch_size, max_no_improvement=10, verbose=0).fit(Vehicle_Descriptors)
        #self.k_means_dic['random'] = KMeans(init='random', n_clusters=self.n_Clusters, n_init=10, max_iter=2000).fit(Vehicle_Descriptors)
        # Definicion de centroides iniciales mediante analisis de componentes principales
 
        pca = PCA(n_components=self.n_Clusters).fit(Vehicle_Descriptors)
        # Entrenar K means utilizando los centroides definidos
        self.k_means_dic['PCA-Components'] = MiniBatchKMeans(init=pca.components_, n_clusters=self.n_Clusters, n_init=1, batch_size=batch_size, max_no_improvement=10, verbose=0).fit(Vehicle_Descriptors)
        #self.k_means_dic['PCA-Components'] = KMeans(init=pca.components_, n_clusters=self.n_Clusters, n_init=1, max_iter=2000).fit(Vehicle_Descriptors)
        print "Finalizado.."


    # Evaluar Kmeans y crear histogramas
    def EvaluateKmeans_CreateHistograms(self, k_means, vehicle_descriptors, descriptors_labels):
        """ Evalua los  k means ya entrenados y entrega el indice del conjunto al cual pertenece
        Keyword arguments:
        vehicle_descriptors -- matriz con descriptores de los vehiculos  de dimensiones [numero_descriptores,dimensiones_descritor]
        descriptors_labels --  arreglo tipo columna con el numero de descriptores por cada vehiculo
        """
        # evaluar Kmeans
        k_result = k_means.predict(vehicle_descriptors)
        # concatenar un 0 a los descriptors_labels para separar las caracteristicas de cada muestre y crear los histogramas
        c_limits = np.concatenate((np.zeros(1), np.cumsum(descriptors_labels)), axis=0)
        # crear matriz de ceros para guardar las  caracteristicas  de entrenamiento de los svms
        svm_input = np.zeros((descriptors_labels.__len__(), self.n_Clusters))
        # iterar sobre los decriptors labels:
        for i in range(1, len(c_limits)):
            # crear los histogramas
            top = int(c_limits[i-1])
            bottom = int(c_limits[i])
            sample_descriptors, bins = np.histogram(k_result[top:bottom], bins=self.histogram_index)
            # guardar el histograma en la matriz de ceros
            svm_input[i-1, :] = sample_descriptors     #retornar la matriz de caracteristicas
        return  svm_input

    # calcular tasa de acierto de los clasificadores
    def Calculate_Accuracy_Classifiers(self, vehicle_descriptors, descriptors_labels, vehicle_labels):
        """Retorna  la tasa de acierto de los clasificadores entrenados
        Argumentos:
        vehicle_descriptors -- arreglo con los descriptores de los vehiculos el cual debe tener dimensiones
        [sum(descriptors_labels), Numero_caracteristicas_descriptor]
        descriptors_labels --  arreglo tipo columna con el numero de descriptores por cada vehiculo
        vehicle_labels -- arreglo tipo columna con las etiquetas de los vehiculos a utilizar para entrenar
        """
        # cacular el porcentaje de acierto del clasificador de 0 a 1
        Acc_Dict = {}
        for key, kmean in self.k_means_dic.iteritems():
            # evaluar K - means
            svm_input = self.EvaluateKmeans_CreateHistograms(kmean, vehicle_descriptors, descriptors_labels)
            for keysvm, svms in self.SVM_dic.iteritems():
                # evaluar unicamente en que el K means coincida con el SVM
                if key == keysvm[0]:
                    # evaluar SVM
                    Result = svms.predict(svm_input)
                    # calcular el porcentaje de acierto del clasificador
                    acc = accuracy_score(Result, vehicle_labels)
                    Acc_Dict[keysvm] = acc
        return Acc_Dict






    # Guardar modelos entrenados
    def SaveModels(self, path):
        """ Guardar los modelos de k means y maquinas de soporte vectorial entrenadas, en caso de no hber sido entrenadas o  cargadas no se guardara nada
        Argumentos:
        path -- Ruta a la carpeta donde se desea guardar los clasificadores, en caso de no existir sera creada
        """
        create_directory(path)
        for key, kmean in self.k_means_dic.iteritems():
            p = path +"/"+key+".pkl"
            joblib.dump(kmean, p)

        for keysvm, svms in self.SVM_dic.iteritems():
            p = path +"/"+keysvm[0]+'_'+keysvm[1]+".pkl"
            joblib.dump(svms, p)


    @staticmethod
    # Cargar modelos entrenados
    def LoadModels(path):
        """ cargar modelos preentrenados y entrega una instalcia de un objeto   Vehicle_Category_Classifier
        Argumentos:
        path -- Ruta a la carpeta donde se almacenados los clasificadores
        """

        k_m = ['k-means++', 'random', 'PCA-Components']
        s_vm = ['lin-SVC', 'rbf', 'lin', 'poly']
        k_means_dic = {}
        n_clusters = 0
        n_classes = 0
        SVM_dic = {}
        for k in k_m:
            print path + '/' + k+'.pkl'
            k_means_dic[k] = joblib.load(path + '/' + k+'.pkl')
            n_clusters = k_means_dic[k].n_clusters

            for svm in s_vm:
                SVM_dic[(k, svm)] = joblib.load(path + '/' + k+'_'+svm+'.pkl')
                n_classes = SVM_dic[(k, svm)].classes_.size
        
        r = Vehicle_Category_Classifier(n_classes=n_classes, n_clusters=n_clusters)
        r.SVM_dic = SVM_dic
        r.k_means_dic = k_means_dic
        return r

    # Mostrar matrices de confusion para cada uno de los casos
    def show_cofusionmatrix_classifiers(self, vehicle_descriptors, descriptors_labels, vehicle_labels, classnames, Normalize=False):
        """Muestra las matrices de confusion de los clasificadores entrenados
        Argumentos:
        vehicle_descriptors -- arreglo con los descriptores de los vehiculos el cual debe tener dimensiones
        [sum(descriptors_labels), Numero_caracteristicas_descriptor]
        descriptors_labels --  arreglo tipo columna con el numero de descriptores por cada vehiculo
        vehicle_labels -- arreglo tipo columna con las etiquetas de los vehiculos a utilizar para entrenar
        Normalize -- mostrar los porcentajes de la matriz de confusion  normalizada, por defecto no se normaliza(False)
        """
        # contador del numero de subplots
        cnt = 0
        # iterar sobre cada  k - means y svms
        for key, kmean in self.k_means_dic.iteritems():
            # evaluar K - means
            svm_input = self.EvaluateKmeans_CreateHistograms(kmean, vehicle_descriptors, descriptors_labels)
            for keysvm, svms in self.SVM_dic.iteritems():
                # evaluar unicamente en que el K means coincida con el SVM
                if key == keysvm[0]:
                    # evaluar SVM
                    Result = svms.predict(svm_input)
                    # crear matriz de confusion
                    cm = confusion_matrix(Result, vehicle_labels)
                    # aumentar contador
                    cnt = cnt+1
                    # si hay mas de 6 matrices de confusion crear una nueva figura
                    if cnt > 6:
                        cnt = 1
                        plt.figure()
                    sp = 230 + cnt
                    #  seleccionar subplot y mostrar matriz de confusion
                    plt.subplot(sp)
                    plot_confusion_matrix(cm, classnames, title=str(keysvm), normalize=Normalize)
        plt.show()

        # Mostrar matrices de confusion para cada uno de los casosdef predict(self, vehicle_descriptors, descriptors_labels, kmeans_key, svm_key):
        svm_input = self.EvaluateKmeans_CreateHistograms(self.k_means_dic[kmeans_key], vehicle_descriptors, descriptors_labels)
        Result = self.SVM_dic[(kmeans_key, svm_key)].predict(svm_input)
        return Result



    # mapear muestras para seleccion de descriptores y ajustes
    def Clusterize_and_showTSNE(self, vehicle_descriptors, descriptors_labels, vehicle_labels, dim=2):
        """ Mapear utilizando T-SNE y mostrar
        Argumentos:
        vehicle_descriptors -- arreglo con los descriptores de los vehiculos el cual debe tener dimensiones
        [sum(descriptors_labels), Numero_caracteristicas_descriptor]
        descriptors_labels --  arreglo tipo columna con el numero de descriptores por cada vehiculo
        vehicle_labels -- arreglo tipo columna con las etiquetas de los vehiculos a utilizar para entrenar
        dim -- numero de dimensiones en las cuales mapear los datos por  defecto 2
        """
        tsne = manifold.TSNE(n_components=2, init='pca', random_state=0)
        cnt = 131
        for key, kmean in self.k_means_dic.iteritems():
            # evaluar K - means
            svm_input = self.EvaluateKmeans_CreateHistograms(kmean, vehicle_descriptors, descriptors_labels)
            X_tsne = tsne.fit_transform(svm_input)
            plot_embedding(X_tsne, labels, key, subplot=cnt)
            cnt = cnt+1
        plt.show()

    # Entrenar seleccionando muestras para entrenamiento y validacion y mostrar resultados
    def divide_samples_train_and_test(self, vehicle_descriptors, descriptors_labels, vehicle_labels, kmean='k-means++', test_size=0.4):
        """dividir el dataset en subsets de prueba y validacion
        Argumentos:
        vehicle_descriptors -- arreglo con los descriptores de los vehiculos el cual debe tener dimensiones
        [sum(descriptors_labels), Numero_caracteristicas_descriptor]
        descriptors_labels --  arreglo tipo columna con el numero de descriptores por cada vehiculo
        vehicle_labels -- arreglo tipo columna con las etiquetas de los vehiculos a utilizar para entrenar
        kmean -- nombre del kmeans a utilizar, si no se ha entrenado aun se entrenan
        test_size --  define que porcentaje de muestras se usan para validacion definido de 0 a 1, por defecto es 0.4 (40%)
        """
        # se verifica que los kmeans esten entrenados, en caso de que no se entrenan
        if self.k_means_dic == {}:
            self.trainKmeans(vehicle_descriptors)
        # se evaluan los datos el   K-mean seleccionado  y se generan los histogramas
        svm_input = self.EvaluateKmeans_CreateHistograms(self.k_means_dic[kmean], vehicle_descriptors, descriptors_labels)
            # Se entrenan los SVM con los histogramas generados
        return train_test_split(svm_input, vehicle_labels, test_size=test_size, random_state=0)
        
# graficar descriptores mapeados
def plot_embedding(X, target, title=None, subplot=111):
    """  graficar datos mapeados en dos dimensiones
    Argumentos:
    X -- arreglo con datos mapeados en dos dimensiones de tamano [numerodemuestras,2]
    title -- titulo de la figura a mostrar
    subplot -- numero del subplot donde mostrar por defecto 111
    """
    x_min, x_max = np.min(X, 0), np.max(X, 0)
    X = (X - x_min) / (x_max - x_min)

    ax = plt.subplot(subplot)
    for i in range(X.shape[0]):
        plt.text(X[i, 0], X[i, 1], str(target[i]),
                 color=plt.cm.Set1(target[i] / 10.),
                 fontdict={'weight': 'bold', 'size': 9})

    plt.xticks([]), plt.yticks([])
    if title is not None:
        plt.title(title)
    

# graficar matriz de confusion
def plot_confusion_matrix(cm, classes, normalize=True, title='Confusion matrix', cmap=plt.cm.Blues):
    """
    protea matrices de confusion
    Argumentos:
    cm -- matriz de confusion
    normalize -- normalizar la matriz de confusion, por defecto se normaliza (True)
    title -- titulo de la matriz de confusion
    cmap -- colores a aplicar a la matriz de confusion, por defecto azules
    """
    plt.imshow(cm, interpolation='nearest', cmap=cmap)
    plt.title(title)
    tick_marks = np.arange(len(classes))
    plt.xticks(tick_marks, classes, rotation=45)
    plt.yticks(tick_marks, classes)

    if normalize:
        cm = cm.astype('float') / cm.sum(axis=1)[:, np.newaxis]
        cm=100*np.around(cm, 2)
    thresh = cm.max() / 2.
    for i, j in itertools.product(range(cm.shape[0]), range(cm.shape[1])):
        plt.text(j, i, cm[i, j],
                 horizontalalignment="center",
                 color="white" if cm[i, j] > thresh else "black")

    #    plt.tight_layout()
    #plt.ylabel('True label')
    #plt.xlabel('Predicted label')

# leer las muestras de diferentes carpetas y recolectarlas  para su uso
def read_and_return_data(des=1):
    """
    leer  descriptores y sus etiquetas desde archivo
    Argumentos:
    des -- si es 1 busca descriptores con nombre FPFH si es 0 busca descriptores con nombre NARF 
    Returns:
    d -- descriptores
    dc -- numero de descriptores por vehiculo
    l -- etiquetas de los vehiculos
    """
    path1=tkFileDialog.askdirectory(initialdir='/home/ssi_ralvarez/Documentos/all_project/Repos/Etiquetado')
    dir_list = os.listdir(path1)
    l = []
    dc = []
    d = []
    for dir_a in dir_list:
        l.extend(np.loadtxt(path1+'/'+dir_a+'/data.txt', usecols=(1, )))
        dc.extend(np.loadtxt(path1+'/'+dir_a+'/Descriptores_Cluster.txt'))
        if des:
            d.extend(np.loadtxt(path1+'/'+dir_a+'/FPFH.pcd', skiprows=11))
            #Data=np.array(np.loadtxt(path1+'/'+dir_a+'/FPFH.pcd',skiprows=11))
        else:
            d.extend(np.loadtxt(path1+'/'+dir_a+'/Narf.pcd', skiprows=11))
            #Data=np.array(np.loadtxt(path1+'/'+dir_a+'/Narf.pcd',skiprows=11))
    d = np.array(d)
    return d, dc, l
# verificar si existe un directorio, en caso contrario lo crea
def create_directory(route):
    """   verifica la existencia de la carpeta en la direccion definida, en caso de que no exista la crea
        Argumentos:
        route -- direccion donde se desea crear la carpeta
        """
    if not os.path.exists(route):
        os.makedirs(route)



if __name__ == '__main__':
    import tkFileDialog
    Data, descriptors_count, labels = read_and_return_data(des=1)
    #path1=tkFileDialog.askdirectory(initialdir='/home/ssi_ralvarez/Documentos/all_project/Repos/Etiquetado')
    #Data=np.array(np.loadtxt(path1+'/Narf.pcd',skiprows=11))
    #Data=np.array(np.loadtxt(path1+'/FPFH.pcd',skiprows=11))
    #labels=np.loadtxt(path1+'/data.txt',usecols=(1,))
    #descriptors_count=np.loadtxt(path1+'/Descriptores_Cluster.txt')
    #Data=np.array(np.loadtxt('/home/ssi_ralvarez/Documentos/all_project/Repos/K_means/DEscriptors/FPFH/N0/FPFH.pcd',skiprows=11))
    #labels=np.loadtxt('/home/ssi_ralvarez/Documentos/all_project/Repos/K_means/DEscriptors/FPFH/N0/FPFH_final.txt')

    clasifier=Vehicle_Category_Classifier(n_classes=3,n_clusters=20)
    clasifier.train(Data,descriptors_count,labels,class_weight={0:1,1:1,2:1})
    clssnames=['noVeh','auto','Cami','bus'] 
    clasifier.Clusterize_and_showTSNE(Data,descriptors_count,labels)
    clasifier.show_cofusionmatrix_classifiers(Data,descriptors_count,labels,clssnames)
    #print clasifier.Calculate_Accuracy_Classifiers(Data,descriptors_count,labels)
    #clasifier.SaveModels('prueba')

    """
    clasifier=Vehicle_Category_Classifier(n_classes=3,n_clusters=10)
    Data,descriptors_count,labels = read_and_return_data(des=0)
    a,b,c,d = clasifier.divide_samples_train_and_test(Data,descriptors_count,labels)
    claifier.train_svm_kmeans_selected('k-means++',a,c)
    """
    #path1=tkFileDialog.askdirectory(initialdir='/home/ssi_ralvarez/Documentos/all_project/Repos/Etiquetado')
    #Data=np.array(np.loadtxt(path1+'/Narf.pcd',skiprows=11))
    #Data=np.array(np.loadtxt(path1+'/FPFH.pcd',skiprows=11))
    #labels=np.loadtxt(path1+'/data.txt',usecols=(1,))
    #descriptors_count=np.loadtxt(path1+'/Descriptores_Cluster.txt')

    """
    model=Vehicle_Category_Classifier.LoadModels('prueba')
    clssnames=['noVeh','auto','Cami','bus']
    clssnames=['noVeh','auto','Cami']
    model.Clusterize_and_showTSNE(Data,descriptors_count,labels)
    model.show_cofusionmatrix_classifiers(Data,descriptors_count,labels,clssnames,Normalize=False)

#    result=model.predict(Data,descriptors_count,'k-means++','rbf')
    his,bins= np.histogram(result, bins=range(4))
    for  i in range(len(clssnames)):
        print clssnames[i],": ",his[i]
    #print(result)
    """